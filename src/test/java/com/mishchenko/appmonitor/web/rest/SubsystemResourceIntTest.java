package com.mishchenko.appmonitor.web.rest;

import static com.mishchenko.appmonitor.web.rest.TestUtil.createFormattingConversionService;
import static com.mishchenko.appmonitor.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import com.mishchenko.appmonitor.AppmonitorApp;
import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.domain.SubsystemInformation;
import com.mishchenko.appmonitor.domain.enumeration.SubsystemType;
import com.mishchenko.appmonitor.repository.SubsystemRepository;
import com.mishchenko.appmonitor.service.SubsystemService;
import com.mishchenko.appmonitor.service.dto.SubsystemDTO;
import com.mishchenko.appmonitor.service.mapper.SubsystemMapper;
import com.mishchenko.appmonitor.web.rest.errors.ExceptionTranslator;
import com.mishchenko.appmonitor.zos.service.SubsystemUpdateService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the SubsystemResource REST controller.
 *
 * @see SubsystemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppmonitorApp.class)
public class SubsystemResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final SubsystemType DEFAULT_SUBSYSTEM_TYPE = SubsystemType.IMS;
    private static final SubsystemType UPDATED_SUBSYSTEM_TYPE = SubsystemType.MQ;

    private static final Integer DEFAULT_REFRESH_RATE = 1;
    private static final Integer UPDATED_REFRESH_RATE = 2;

    private static final ZonedDateTime DEFAULT_UPDATED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L),
            ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final SubsystemInformation DEFAULT_INFORMATION = null;
    private static final SubsystemInformation UPDATED_INFORMATION = null;

    @Autowired
    private SubsystemRepository subsystemRepository;

    @Autowired
    private SubsystemMapper subsystemMapper;

    @Autowired
    private SubsystemService subsystemService;

    @Autowired
    private SubsystemUpdateService subsystemUpdateService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSubsystemMockMvc;

    private Subsystem subsystem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubsystemResource subsystemResource = new SubsystemResource(subsystemService);
        this.restSubsystemMockMvc = MockMvcBuilders.standaloneSetup(subsystemResource)
                .setCustomArgumentResolvers(pageableArgumentResolver).setControllerAdvice(exceptionTranslator)
                .setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
                .build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it, if
     * they test an entity which requires the current entity.
     */
    public static Subsystem createEntity(EntityManager em) {
        Subsystem subsystem = new Subsystem().name(DEFAULT_NAME).subsystemType(DEFAULT_SUBSYSTEM_TYPE)
                .refreshRate(DEFAULT_REFRESH_RATE).updatedAt(DEFAULT_UPDATED_AT).information(DEFAULT_INFORMATION);
        return subsystem;
    }

    @Before
    public void initTest() {
        subsystem = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubsystem() throws Exception {
        int databaseSizeBeforeCreate = subsystemRepository.findAll().size();

        // Create the Subsystem
        SubsystemDTO subsystemDTO = subsystemMapper.toDto(subsystem);
        restSubsystemMockMvc.perform(post("/api/subsystems").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subsystemDTO))).andExpect(status().isCreated());

        // Validate the Subsystem in the database
        List<Subsystem> subsystemList = subsystemRepository.findAll();
        assertThat(subsystemList).hasSize(databaseSizeBeforeCreate + 1);
        Subsystem testSubsystem = subsystemList.get(subsystemList.size() - 1);
        assertThat(testSubsystem.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSubsystem.getSubsystemType()).isEqualTo(DEFAULT_SUBSYSTEM_TYPE);
        assertThat(testSubsystem.getRefreshRate()).isEqualTo(DEFAULT_REFRESH_RATE);
        assertThat(testSubsystem.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
        assertThat(testSubsystem.getInformation()).isEqualTo(DEFAULT_INFORMATION);
    }

    @Test
    @Transactional
    public void createSubsystemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subsystemRepository.findAll().size();

        // Create the Subsystem with an existing ID
        subsystem.setId(1L);
        SubsystemDTO subsystemDTO = subsystemMapper.toDto(subsystem);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubsystemMockMvc.perform(post("/api/subsystems").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subsystemDTO))).andExpect(status().isBadRequest());

        // Validate the Subsystem in the database
        List<Subsystem> subsystemList = subsystemRepository.findAll();
        assertThat(subsystemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSubsystems() throws Exception {
        // Initialize the database
        subsystemRepository.saveAndFlush(subsystem);

        // Get all the subsystemList
        restSubsystemMockMvc.perform(get("/api/subsystems?sort=id,desc")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.[*].id").value(hasItem(subsystem.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].subsystemType").value(hasItem(DEFAULT_SUBSYSTEM_TYPE.toString())))
                .andExpect(jsonPath("$.[*].refreshRate").value(hasItem(DEFAULT_REFRESH_RATE)))
                .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(sameInstant(DEFAULT_UPDATED_AT))))
                .andExpect(jsonPath("$.[*].information").value(hasItem(DEFAULT_INFORMATION.toString())));
    }

    @Test
    @Transactional
    public void getSubsystem() throws Exception {
        // Initialize the database
        subsystemRepository.saveAndFlush(subsystem);

        // Get the subsystem
        restSubsystemMockMvc.perform(get("/api/subsystems/{id}", subsystem.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.id").value(subsystem.getId().intValue()))
                .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
                .andExpect(jsonPath("$.subsystemType").value(DEFAULT_SUBSYSTEM_TYPE.toString()))
                .andExpect(jsonPath("$.refreshRate").value(DEFAULT_REFRESH_RATE))
                .andExpect(jsonPath("$.updatedAt").value(sameInstant(DEFAULT_UPDATED_AT)))
                .andExpect(jsonPath("$.information").value(DEFAULT_INFORMATION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubsystem() throws Exception {
        // Get the subsystem
        restSubsystemMockMvc.perform(get("/api/subsystems/{id}", Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubsystem() throws Exception {
        // Initialize the database
        subsystemRepository.saveAndFlush(subsystem);
        int databaseSizeBeforeUpdate = subsystemRepository.findAll().size();

        // Update the subsystem
        Subsystem updatedSubsystem = subsystemRepository.findOne(subsystem.getId());
        // Disconnect from session so that the updates on updatedSubsystem are not
        // directly saved in db
        em.detach(updatedSubsystem);
        updatedSubsystem.name(UPDATED_NAME).subsystemType(UPDATED_SUBSYSTEM_TYPE).refreshRate(UPDATED_REFRESH_RATE)
                .updatedAt(UPDATED_UPDATED_AT).information(UPDATED_INFORMATION);
        SubsystemDTO subsystemDTO = subsystemMapper.toDto(updatedSubsystem);

        restSubsystemMockMvc.perform(put("/api/subsystems").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subsystemDTO))).andExpect(status().isOk());

        // Validate the Subsystem in the database
        List<Subsystem> subsystemList = subsystemRepository.findAll();
        assertThat(subsystemList).hasSize(databaseSizeBeforeUpdate);
        Subsystem testSubsystem = subsystemList.get(subsystemList.size() - 1);
        assertThat(testSubsystem.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSubsystem.getSubsystemType()).isEqualTo(UPDATED_SUBSYSTEM_TYPE);
        assertThat(testSubsystem.getRefreshRate()).isEqualTo(UPDATED_REFRESH_RATE);
        assertThat(testSubsystem.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
        assertThat(testSubsystem.getInformation()).isEqualTo(UPDATED_INFORMATION);
    }

    @Test
    @Transactional
    public void updateNonExistingSubsystem() throws Exception {
        int databaseSizeBeforeUpdate = subsystemRepository.findAll().size();

        // Create the Subsystem
        SubsystemDTO subsystemDTO = subsystemMapper.toDto(subsystem);

        // If the entity doesn't have an ID, it will be created instead of just being
        // updated
        restSubsystemMockMvc.perform(put("/api/subsystems").contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subsystemDTO))).andExpect(status().isCreated());

        // Validate the Subsystem in the database
        List<Subsystem> subsystemList = subsystemRepository.findAll();
        assertThat(subsystemList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSubsystem() throws Exception {
        // Initialize the database
        subsystemRepository.saveAndFlush(subsystem);
        int databaseSizeBeforeDelete = subsystemRepository.findAll().size();

        // Get the subsystem
        restSubsystemMockMvc
                .perform(delete("/api/subsystems/{id}", subsystem.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Subsystem> subsystemList = subsystemRepository.findAll();
        assertThat(subsystemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Subsystem.class);
        Subsystem subsystem1 = new Subsystem();
        subsystem1.setId(1L);
        Subsystem subsystem2 = new Subsystem();
        subsystem2.setId(subsystem1.getId());
        assertThat(subsystem1).isEqualTo(subsystem2);
        subsystem2.setId(2L);
        assertThat(subsystem1).isNotEqualTo(subsystem2);
        subsystem1.setId(null);
        assertThat(subsystem1).isNotEqualTo(subsystem2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubsystemDTO.class);
        SubsystemDTO subsystemDTO1 = new SubsystemDTO();
        subsystemDTO1.setId(1L);
        SubsystemDTO subsystemDTO2 = new SubsystemDTO();
        assertThat(subsystemDTO1).isNotEqualTo(subsystemDTO2);
        subsystemDTO2.setId(subsystemDTO1.getId());
        assertThat(subsystemDTO1).isEqualTo(subsystemDTO2);
        subsystemDTO2.setId(2L);
        assertThat(subsystemDTO1).isNotEqualTo(subsystemDTO2);
        subsystemDTO1.setId(null);
        assertThat(subsystemDTO1).isNotEqualTo(subsystemDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subsystemMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subsystemMapper.fromId(null)).isNull();
    }
}
