package com.mishchenko.appmonitor.web.rest;

import com.mishchenko.appmonitor.AppmonitorApp;

import com.mishchenko.appmonitor.domain.ObjectStatus;
import com.mishchenko.appmonitor.repository.ObjectStatusRepository;
import com.mishchenko.appmonitor.service.ObjectStatusService;
import com.mishchenko.appmonitor.service.dto.ObjectStatusDTO;
import com.mishchenko.appmonitor.service.mapper.ObjectStatusMapper;
import com.mishchenko.appmonitor.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mishchenko.appmonitor.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ObjectStatusResource REST controller.
 *
 * @see ObjectStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppmonitorApp.class)
public class ObjectStatusResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ObjectStatusRepository objectStatusRepository;

    @Autowired
    private ObjectStatusMapper objectStatusMapper;

    @Autowired
    private ObjectStatusService objectStatusService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restObjectStatusMockMvc;

    private ObjectStatus objectStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ObjectStatusResource objectStatusResource = new ObjectStatusResource(objectStatusService);
        this.restObjectStatusMockMvc = MockMvcBuilders.standaloneSetup(objectStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ObjectStatus createEntity(EntityManager em) {
        ObjectStatus objectStatus = new ObjectStatus()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return objectStatus;
    }

    @Before
    public void initTest() {
        objectStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createObjectStatus() throws Exception {
        int databaseSizeBeforeCreate = objectStatusRepository.findAll().size();

        // Create the ObjectStatus
        ObjectStatusDTO objectStatusDTO = objectStatusMapper.toDto(objectStatus);
        restObjectStatusMockMvc.perform(post("/api/object-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the ObjectStatus in the database
        List<ObjectStatus> objectStatusList = objectStatusRepository.findAll();
        assertThat(objectStatusList).hasSize(databaseSizeBeforeCreate + 1);
        ObjectStatus testObjectStatus = objectStatusList.get(objectStatusList.size() - 1);
        assertThat(testObjectStatus.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testObjectStatus.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createObjectStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = objectStatusRepository.findAll().size();

        // Create the ObjectStatus with an existing ID
        objectStatus.setId(1L);
        ObjectStatusDTO objectStatusDTO = objectStatusMapper.toDto(objectStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restObjectStatusMockMvc.perform(post("/api/object-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ObjectStatus in the database
        List<ObjectStatus> objectStatusList = objectStatusRepository.findAll();
        assertThat(objectStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllObjectStatuses() throws Exception {
        // Initialize the database
        objectStatusRepository.saveAndFlush(objectStatus);

        // Get all the objectStatusList
        restObjectStatusMockMvc.perform(get("/api/object-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(objectStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getObjectStatus() throws Exception {
        // Initialize the database
        objectStatusRepository.saveAndFlush(objectStatus);

        // Get the objectStatus
        restObjectStatusMockMvc.perform(get("/api/object-statuses/{id}", objectStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(objectStatus.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingObjectStatus() throws Exception {
        // Get the objectStatus
        restObjectStatusMockMvc.perform(get("/api/object-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateObjectStatus() throws Exception {
        // Initialize the database
        objectStatusRepository.saveAndFlush(objectStatus);
        int databaseSizeBeforeUpdate = objectStatusRepository.findAll().size();

        // Update the objectStatus
        ObjectStatus updatedObjectStatus = objectStatusRepository.findOne(objectStatus.getId());
        // Disconnect from session so that the updates on updatedObjectStatus are not directly saved in db
        em.detach(updatedObjectStatus);
        updatedObjectStatus
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        ObjectStatusDTO objectStatusDTO = objectStatusMapper.toDto(updatedObjectStatus);

        restObjectStatusMockMvc.perform(put("/api/object-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectStatusDTO)))
            .andExpect(status().isOk());

        // Validate the ObjectStatus in the database
        List<ObjectStatus> objectStatusList = objectStatusRepository.findAll();
        assertThat(objectStatusList).hasSize(databaseSizeBeforeUpdate);
        ObjectStatus testObjectStatus = objectStatusList.get(objectStatusList.size() - 1);
        assertThat(testObjectStatus.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testObjectStatus.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingObjectStatus() throws Exception {
        int databaseSizeBeforeUpdate = objectStatusRepository.findAll().size();

        // Create the ObjectStatus
        ObjectStatusDTO objectStatusDTO = objectStatusMapper.toDto(objectStatus);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restObjectStatusMockMvc.perform(put("/api/object-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(objectStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the ObjectStatus in the database
        List<ObjectStatus> objectStatusList = objectStatusRepository.findAll();
        assertThat(objectStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteObjectStatus() throws Exception {
        // Initialize the database
        objectStatusRepository.saveAndFlush(objectStatus);
        int databaseSizeBeforeDelete = objectStatusRepository.findAll().size();

        // Get the objectStatus
        restObjectStatusMockMvc.perform(delete("/api/object-statuses/{id}", objectStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ObjectStatus> objectStatusList = objectStatusRepository.findAll();
        assertThat(objectStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ObjectStatus.class);
        ObjectStatus objectStatus1 = new ObjectStatus();
        objectStatus1.setId(1L);
        ObjectStatus objectStatus2 = new ObjectStatus();
        objectStatus2.setId(objectStatus1.getId());
        assertThat(objectStatus1).isEqualTo(objectStatus2);
        objectStatus2.setId(2L);
        assertThat(objectStatus1).isNotEqualTo(objectStatus2);
        objectStatus1.setId(null);
        assertThat(objectStatus1).isNotEqualTo(objectStatus2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ObjectStatusDTO.class);
        ObjectStatusDTO objectStatusDTO1 = new ObjectStatusDTO();
        objectStatusDTO1.setId(1L);
        ObjectStatusDTO objectStatusDTO2 = new ObjectStatusDTO();
        assertThat(objectStatusDTO1).isNotEqualTo(objectStatusDTO2);
        objectStatusDTO2.setId(objectStatusDTO1.getId());
        assertThat(objectStatusDTO1).isEqualTo(objectStatusDTO2);
        objectStatusDTO2.setId(2L);
        assertThat(objectStatusDTO1).isNotEqualTo(objectStatusDTO2);
        objectStatusDTO1.setId(null);
        assertThat(objectStatusDTO1).isNotEqualTo(objectStatusDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(objectStatusMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(objectStatusMapper.fromId(null)).isNull();
    }
}
