package com.mishchenko.appmonitor.web.rest;

import com.mishchenko.appmonitor.AppmonitorApp;

import com.mishchenko.appmonitor.domain.SubsystemJob;
import com.mishchenko.appmonitor.repository.SubsystemJobRepository;
import com.mishchenko.appmonitor.service.SubsystemJobService;
import com.mishchenko.appmonitor.service.dto.SubsystemJobDTO;
import com.mishchenko.appmonitor.service.mapper.SubsystemJobMapper;
import com.mishchenko.appmonitor.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.mishchenko.appmonitor.web.rest.TestUtil.sameInstant;
import static com.mishchenko.appmonitor.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mishchenko.appmonitor.domain.enumeration.JesJobStatus;
import com.mishchenko.appmonitor.domain.enumeration.SubsystemJobType;
/**
 * Test class for the SubsystemJobResource REST controller.
 *
 * @see SubsystemJobResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppmonitorApp.class)
public class SubsystemJobResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final JesJobStatus DEFAULT_STATUS = JesJobStatus.ACTIVE;
    private static final JesJobStatus UPDATED_STATUS = JesJobStatus.STOPPED;

    private static final SubsystemJobType DEFAULT_TYPE = SubsystemJobType.IMS_CONNECT;
    private static final SubsystemJobType UPDATED_TYPE = SubsystemJobType.IMS_CONTROL_REGION;

    private static final ZonedDateTime DEFAULT_STARTED_AT = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTED_AT = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_LOG = "AAAAAAAAAA";
    private static final String UPDATED_LOG = "BBBBBBBBBB";

    @Autowired
    private SubsystemJobRepository subsystemJobRepository;

    @Autowired
    private SubsystemJobMapper subsystemJobMapper;

    @Autowired
    private SubsystemJobService subsystemJobService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSubsystemJobMockMvc;

    private SubsystemJob subsystemJob;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubsystemJobResource subsystemJobResource = new SubsystemJobResource(subsystemJobService);
        this.restSubsystemJobMockMvc = MockMvcBuilders.standaloneSetup(subsystemJobResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubsystemJob createEntity(EntityManager em) {
        SubsystemJob subsystemJob = new SubsystemJob()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .status(DEFAULT_STATUS)
            .type(DEFAULT_TYPE)
            .startedAt(DEFAULT_STARTED_AT)
            .log(DEFAULT_LOG);
        return subsystemJob;
    }

    @Before
    public void initTest() {
        subsystemJob = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubsystemJob() throws Exception {
        int databaseSizeBeforeCreate = subsystemJobRepository.findAll().size();

        // Create the SubsystemJob
        SubsystemJobDTO subsystemJobDTO = subsystemJobMapper.toDto(subsystemJob);
        restSubsystemJobMockMvc.perform(post("/api/subsystem-jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemJobDTO)))
            .andExpect(status().isCreated());

        // Validate the SubsystemJob in the database
        List<SubsystemJob> subsystemJobList = subsystemJobRepository.findAll();
        assertThat(subsystemJobList).hasSize(databaseSizeBeforeCreate + 1);
        SubsystemJob testSubsystemJob = subsystemJobList.get(subsystemJobList.size() - 1);
        assertThat(testSubsystemJob.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSubsystemJob.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSubsystemJob.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testSubsystemJob.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testSubsystemJob.getStartedAt()).isEqualTo(DEFAULT_STARTED_AT);
        assertThat(testSubsystemJob.getLog()).isEqualTo(DEFAULT_LOG);
    }

    @Test
    @Transactional
    public void createSubsystemJobWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subsystemJobRepository.findAll().size();

        // Create the SubsystemJob with an existing ID
        subsystemJob.setId(1L);
        SubsystemJobDTO subsystemJobDTO = subsystemJobMapper.toDto(subsystemJob);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubsystemJobMockMvc.perform(post("/api/subsystem-jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemJobDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubsystemJob in the database
        List<SubsystemJob> subsystemJobList = subsystemJobRepository.findAll();
        assertThat(subsystemJobList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSubsystemJobs() throws Exception {
        // Initialize the database
        subsystemJobRepository.saveAndFlush(subsystemJob);

        // Get all the subsystemJobList
        restSubsystemJobMockMvc.perform(get("/api/subsystem-jobs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subsystemJob.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].startedAt").value(hasItem(sameInstant(DEFAULT_STARTED_AT))))
            .andExpect(jsonPath("$.[*].log").value(hasItem(DEFAULT_LOG.toString())));
    }

    @Test
    @Transactional
    public void getSubsystemJob() throws Exception {
        // Initialize the database
        subsystemJobRepository.saveAndFlush(subsystemJob);

        // Get the subsystemJob
        restSubsystemJobMockMvc.perform(get("/api/subsystem-jobs/{id}", subsystemJob.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subsystemJob.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.startedAt").value(sameInstant(DEFAULT_STARTED_AT)))
            .andExpect(jsonPath("$.log").value(DEFAULT_LOG.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubsystemJob() throws Exception {
        // Get the subsystemJob
        restSubsystemJobMockMvc.perform(get("/api/subsystem-jobs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubsystemJob() throws Exception {
        // Initialize the database
        subsystemJobRepository.saveAndFlush(subsystemJob);
        int databaseSizeBeforeUpdate = subsystemJobRepository.findAll().size();

        // Update the subsystemJob
        SubsystemJob updatedSubsystemJob = subsystemJobRepository.findOne(subsystemJob.getId());
        // Disconnect from session so that the updates on updatedSubsystemJob are not directly saved in db
        em.detach(updatedSubsystemJob);
        updatedSubsystemJob
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .status(UPDATED_STATUS)
            .type(UPDATED_TYPE)
            .startedAt(UPDATED_STARTED_AT)
            .log(UPDATED_LOG);
        SubsystemJobDTO subsystemJobDTO = subsystemJobMapper.toDto(updatedSubsystemJob);

        restSubsystemJobMockMvc.perform(put("/api/subsystem-jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemJobDTO)))
            .andExpect(status().isOk());

        // Validate the SubsystemJob in the database
        List<SubsystemJob> subsystemJobList = subsystemJobRepository.findAll();
        assertThat(subsystemJobList).hasSize(databaseSizeBeforeUpdate);
        SubsystemJob testSubsystemJob = subsystemJobList.get(subsystemJobList.size() - 1);
        assertThat(testSubsystemJob.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSubsystemJob.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSubsystemJob.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testSubsystemJob.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testSubsystemJob.getStartedAt()).isEqualTo(UPDATED_STARTED_AT);
        assertThat(testSubsystemJob.getLog()).isEqualTo(UPDATED_LOG);
    }

    @Test
    @Transactional
    public void updateNonExistingSubsystemJob() throws Exception {
        int databaseSizeBeforeUpdate = subsystemJobRepository.findAll().size();

        // Create the SubsystemJob
        SubsystemJobDTO subsystemJobDTO = subsystemJobMapper.toDto(subsystemJob);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSubsystemJobMockMvc.perform(put("/api/subsystem-jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemJobDTO)))
            .andExpect(status().isCreated());

        // Validate the SubsystemJob in the database
        List<SubsystemJob> subsystemJobList = subsystemJobRepository.findAll();
        assertThat(subsystemJobList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSubsystemJob() throws Exception {
        // Initialize the database
        subsystemJobRepository.saveAndFlush(subsystemJob);
        int databaseSizeBeforeDelete = subsystemJobRepository.findAll().size();

        // Get the subsystemJob
        restSubsystemJobMockMvc.perform(delete("/api/subsystem-jobs/{id}", subsystemJob.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SubsystemJob> subsystemJobList = subsystemJobRepository.findAll();
        assertThat(subsystemJobList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubsystemJob.class);
        SubsystemJob subsystemJob1 = new SubsystemJob();
        subsystemJob1.setId(1L);
        SubsystemJob subsystemJob2 = new SubsystemJob();
        subsystemJob2.setId(subsystemJob1.getId());
        assertThat(subsystemJob1).isEqualTo(subsystemJob2);
        subsystemJob2.setId(2L);
        assertThat(subsystemJob1).isNotEqualTo(subsystemJob2);
        subsystemJob1.setId(null);
        assertThat(subsystemJob1).isNotEqualTo(subsystemJob2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubsystemJobDTO.class);
        SubsystemJobDTO subsystemJobDTO1 = new SubsystemJobDTO();
        subsystemJobDTO1.setId(1L);
        SubsystemJobDTO subsystemJobDTO2 = new SubsystemJobDTO();
        assertThat(subsystemJobDTO1).isNotEqualTo(subsystemJobDTO2);
        subsystemJobDTO2.setId(subsystemJobDTO1.getId());
        assertThat(subsystemJobDTO1).isEqualTo(subsystemJobDTO2);
        subsystemJobDTO2.setId(2L);
        assertThat(subsystemJobDTO1).isNotEqualTo(subsystemJobDTO2);
        subsystemJobDTO1.setId(null);
        assertThat(subsystemJobDTO1).isNotEqualTo(subsystemJobDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subsystemJobMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subsystemJobMapper.fromId(null)).isNull();
    }
}
