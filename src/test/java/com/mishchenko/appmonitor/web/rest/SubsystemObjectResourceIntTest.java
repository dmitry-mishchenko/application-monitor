package com.mishchenko.appmonitor.web.rest;

import com.mishchenko.appmonitor.AppmonitorApp;

import com.mishchenko.appmonitor.domain.SubsystemObject;
import com.mishchenko.appmonitor.repository.SubsystemObjectRepository;
import com.mishchenko.appmonitor.service.SubsystemObjectService;
import com.mishchenko.appmonitor.service.dto.SubsystemObjectDTO;
import com.mishchenko.appmonitor.service.mapper.SubsystemObjectMapper;
import com.mishchenko.appmonitor.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mishchenko.appmonitor.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SubsystemObjectResource REST controller.
 *
 * @see SubsystemObjectResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppmonitorApp.class)
public class SubsystemObjectResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_ACTIVE = false;
    private static final Boolean UPDATED_IS_ACTIVE = true;

    @Autowired
    private SubsystemObjectRepository subsystemObjectRepository;

    @Autowired
    private SubsystemObjectMapper subsystemObjectMapper;

    @Autowired
    private SubsystemObjectService subsystemObjectService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restSubsystemObjectMockMvc;

    private SubsystemObject subsystemObject;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubsystemObjectResource subsystemObjectResource = new SubsystemObjectResource(subsystemObjectService);
        this.restSubsystemObjectMockMvc = MockMvcBuilders.standaloneSetup(subsystemObjectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubsystemObject createEntity(EntityManager em) {
        SubsystemObject subsystemObject = new SubsystemObject()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .isActive(DEFAULT_IS_ACTIVE);
        return subsystemObject;
    }

    @Before
    public void initTest() {
        subsystemObject = createEntity(em);
    }

    @Test
    @Transactional
    public void createSubsystemObject() throws Exception {
        int databaseSizeBeforeCreate = subsystemObjectRepository.findAll().size();

        // Create the SubsystemObject
        SubsystemObjectDTO subsystemObjectDTO = subsystemObjectMapper.toDto(subsystemObject);
        restSubsystemObjectMockMvc.perform(post("/api/subsystem-objects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemObjectDTO)))
            .andExpect(status().isCreated());

        // Validate the SubsystemObject in the database
        List<SubsystemObject> subsystemObjectList = subsystemObjectRepository.findAll();
        assertThat(subsystemObjectList).hasSize(databaseSizeBeforeCreate + 1);
        SubsystemObject testSubsystemObject = subsystemObjectList.get(subsystemObjectList.size() - 1);
        assertThat(testSubsystemObject.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSubsystemObject.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testSubsystemObject.isIsActive()).isEqualTo(DEFAULT_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void createSubsystemObjectWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subsystemObjectRepository.findAll().size();

        // Create the SubsystemObject with an existing ID
        subsystemObject.setId(1L);
        SubsystemObjectDTO subsystemObjectDTO = subsystemObjectMapper.toDto(subsystemObject);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubsystemObjectMockMvc.perform(post("/api/subsystem-objects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemObjectDTO)))
            .andExpect(status().isBadRequest());

        // Validate the SubsystemObject in the database
        List<SubsystemObject> subsystemObjectList = subsystemObjectRepository.findAll();
        assertThat(subsystemObjectList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllSubsystemObjects() throws Exception {
        // Initialize the database
        subsystemObjectRepository.saveAndFlush(subsystemObject);

        // Get all the subsystemObjectList
        restSubsystemObjectMockMvc.perform(get("/api/subsystem-objects?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subsystemObject.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].isActive").value(hasItem(DEFAULT_IS_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getSubsystemObject() throws Exception {
        // Initialize the database
        subsystemObjectRepository.saveAndFlush(subsystemObject);

        // Get the subsystemObject
        restSubsystemObjectMockMvc.perform(get("/api/subsystem-objects/{id}", subsystemObject.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(subsystemObject.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.isActive").value(DEFAULT_IS_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingSubsystemObject() throws Exception {
        // Get the subsystemObject
        restSubsystemObjectMockMvc.perform(get("/api/subsystem-objects/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubsystemObject() throws Exception {
        // Initialize the database
        subsystemObjectRepository.saveAndFlush(subsystemObject);
        int databaseSizeBeforeUpdate = subsystemObjectRepository.findAll().size();

        // Update the subsystemObject
        SubsystemObject updatedSubsystemObject = subsystemObjectRepository.findOne(subsystemObject.getId());
        // Disconnect from session so that the updates on updatedSubsystemObject are not directly saved in db
        em.detach(updatedSubsystemObject);
        updatedSubsystemObject
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .isActive(UPDATED_IS_ACTIVE);
        SubsystemObjectDTO subsystemObjectDTO = subsystemObjectMapper.toDto(updatedSubsystemObject);

        restSubsystemObjectMockMvc.perform(put("/api/subsystem-objects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemObjectDTO)))
            .andExpect(status().isOk());

        // Validate the SubsystemObject in the database
        List<SubsystemObject> subsystemObjectList = subsystemObjectRepository.findAll();
        assertThat(subsystemObjectList).hasSize(databaseSizeBeforeUpdate);
        SubsystemObject testSubsystemObject = subsystemObjectList.get(subsystemObjectList.size() - 1);
        assertThat(testSubsystemObject.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSubsystemObject.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testSubsystemObject.isIsActive()).isEqualTo(UPDATED_IS_ACTIVE);
    }

    @Test
    @Transactional
    public void updateNonExistingSubsystemObject() throws Exception {
        int databaseSizeBeforeUpdate = subsystemObjectRepository.findAll().size();

        // Create the SubsystemObject
        SubsystemObjectDTO subsystemObjectDTO = subsystemObjectMapper.toDto(subsystemObject);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSubsystemObjectMockMvc.perform(put("/api/subsystem-objects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subsystemObjectDTO)))
            .andExpect(status().isCreated());

        // Validate the SubsystemObject in the database
        List<SubsystemObject> subsystemObjectList = subsystemObjectRepository.findAll();
        assertThat(subsystemObjectList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteSubsystemObject() throws Exception {
        // Initialize the database
        subsystemObjectRepository.saveAndFlush(subsystemObject);
        int databaseSizeBeforeDelete = subsystemObjectRepository.findAll().size();

        // Get the subsystemObject
        restSubsystemObjectMockMvc.perform(delete("/api/subsystem-objects/{id}", subsystemObject.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<SubsystemObject> subsystemObjectList = subsystemObjectRepository.findAll();
        assertThat(subsystemObjectList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubsystemObject.class);
        SubsystemObject subsystemObject1 = new SubsystemObject();
        subsystemObject1.setId(1L);
        SubsystemObject subsystemObject2 = new SubsystemObject();
        subsystemObject2.setId(subsystemObject1.getId());
        assertThat(subsystemObject1).isEqualTo(subsystemObject2);
        subsystemObject2.setId(2L);
        assertThat(subsystemObject1).isNotEqualTo(subsystemObject2);
        subsystemObject1.setId(null);
        assertThat(subsystemObject1).isNotEqualTo(subsystemObject2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubsystemObjectDTO.class);
        SubsystemObjectDTO subsystemObjectDTO1 = new SubsystemObjectDTO();
        subsystemObjectDTO1.setId(1L);
        SubsystemObjectDTO subsystemObjectDTO2 = new SubsystemObjectDTO();
        assertThat(subsystemObjectDTO1).isNotEqualTo(subsystemObjectDTO2);
        subsystemObjectDTO2.setId(subsystemObjectDTO1.getId());
        assertThat(subsystemObjectDTO1).isEqualTo(subsystemObjectDTO2);
        subsystemObjectDTO2.setId(2L);
        assertThat(subsystemObjectDTO1).isNotEqualTo(subsystemObjectDTO2);
        subsystemObjectDTO1.setId(null);
        assertThat(subsystemObjectDTO1).isNotEqualTo(subsystemObjectDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subsystemObjectMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subsystemObjectMapper.fromId(null)).isNull();
    }
}
