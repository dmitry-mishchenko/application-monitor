package com.mishchenko.appmonitor.zos.util;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import com.mishchenko.appmonitor.domain.Subsystem;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

public class ZJobUtilIntTest {

    private ZConnectionParameters connectionParameters;

    @Before
    public void createParameters() {
        this.connectionParameters = new ZConnectionParameters();
        connectionParameters.setHostname("172.20.2.116");
        connectionParameters.setUserId("USER20A");
        connectionParameters.setPassword("USER20A");
        connectionParameters.setJobClass("A");
        connectionParameters.setJobAccountingInfo("");
        connectionParameters.setSubsystem(new Subsystem().name("IVP1"));
    }

    @Test
    public void testPrepareJesJobStatusesJcl() throws Exception {
        List<String> jobNames = new LinkedList<>();
        jobNames.add("IMS13CR1");
        InputStream inputStream = ZJobUtil.prepareJesJobStatusesJcl(jobNames, this.connectionParameters);
        assertEquals(IOUtils.toString(inputStream, "UTF-8").contains("IMS13CR1"), true);
    }
}
