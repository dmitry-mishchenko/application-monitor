package com.mishchenko.appmonitor.zos.connection;

import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;

import org.junit.Before;

public class JesConnectionIntTest {

    private ZConnectionParameters connectionParameters;

    private static final String testJobName = "IMS13CR1";

    @Before
    public void createParameters() {
        this.connectionParameters = new ZConnectionParameters();
        connectionParameters.setHostname("172.20.2.116");
        connectionParameters.setUserId("USER20A");
        connectionParameters.setPassword("USER20A");
        connectionParameters.setJobClass("A");
        connectionParameters.setJobAccountingInfo("");
        connectionParameters.setSubsystem(new Subsystem().name("IVP1"));
    }
}