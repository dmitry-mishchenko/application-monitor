/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { AppmonitorTestModule } from '../../../test.module';
import { ObjectTypeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/object-type/object-type-delete-dialog.component';
import { ObjectTypeService } from '../../../../../../main/webapp/app/entities/object-type/object-type.service';

describe('Component Tests', () => {

    describe('ObjectType Management Delete Component', () => {
        let comp: ObjectTypeDeleteDialogComponent;
        let fixture: ComponentFixture<ObjectTypeDeleteDialogComponent>;
        let service: ObjectTypeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [ObjectTypeDeleteDialogComponent],
                providers: [
                    ObjectTypeService
                ]
            })
            .overrideTemplate(ObjectTypeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ObjectTypeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ObjectTypeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
