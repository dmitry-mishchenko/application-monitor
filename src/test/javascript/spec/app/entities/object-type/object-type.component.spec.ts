/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppmonitorTestModule } from '../../../test.module';
import { ObjectTypeComponent } from '../../../../../../main/webapp/app/entities/object-type/object-type.component';
import { ObjectTypeService } from '../../../../../../main/webapp/app/entities/object-type/object-type.service';
import { ObjectType } from '../../../../../../main/webapp/app/entities/object-type/object-type.model';

describe('Component Tests', () => {

    describe('ObjectType Management Component', () => {
        let comp: ObjectTypeComponent;
        let fixture: ComponentFixture<ObjectTypeComponent>;
        let service: ObjectTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [ObjectTypeComponent],
                providers: [
                    ObjectTypeService
                ]
            })
            .overrideTemplate(ObjectTypeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ObjectTypeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ObjectTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ObjectType(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.objectTypes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
