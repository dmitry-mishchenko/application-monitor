/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppmonitorTestModule } from '../../../test.module';
import { ObjectStatusComponent } from '../../../../../../main/webapp/app/entities/object-status/object-status.component';
import { ObjectStatusService } from '../../../../../../main/webapp/app/entities/object-status/object-status.service';
import { ObjectStatus } from '../../../../../../main/webapp/app/entities/object-status/object-status.model';

describe('Component Tests', () => {

    describe('ObjectStatus Management Component', () => {
        let comp: ObjectStatusComponent;
        let fixture: ComponentFixture<ObjectStatusComponent>;
        let service: ObjectStatusService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [ObjectStatusComponent],
                providers: [
                    ObjectStatusService
                ]
            })
            .overrideTemplate(ObjectStatusComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ObjectStatusComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ObjectStatusService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ObjectStatus(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.objectStatuses[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
