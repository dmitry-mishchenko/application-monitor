/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemJobDetailComponent } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job-detail.component';
import { SubsystemJobService } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job.service';
import { SubsystemJob } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job.model';

describe('Component Tests', () => {

    describe('SubsystemJob Management Detail Component', () => {
        let comp: SubsystemJobDetailComponent;
        let fixture: ComponentFixture<SubsystemJobDetailComponent>;
        let service: SubsystemJobService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemJobDetailComponent],
                providers: [
                    SubsystemJobService
                ]
            })
            .overrideTemplate(SubsystemJobDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemJobDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemJobService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SubsystemJob(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.subsystemJob).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
