/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemJobDialogComponent } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job-dialog.component';
import { SubsystemJobService } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job.service';
import { SubsystemJob } from '../../../../../../main/webapp/app/entities/subsystem-job/subsystem-job.model';
import { SubsystemService } from '../../../../../../main/webapp/app/entities/subsystem';

describe('Component Tests', () => {

    describe('SubsystemJob Management Dialog Component', () => {
        let comp: SubsystemJobDialogComponent;
        let fixture: ComponentFixture<SubsystemJobDialogComponent>;
        let service: SubsystemJobService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemJobDialogComponent],
                providers: [
                    SubsystemService,
                    SubsystemJobService
                ]
            })
            .overrideTemplate(SubsystemJobDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemJobDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemJobService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SubsystemJob(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.subsystemJob = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'subsystemJobListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SubsystemJob();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.subsystemJob = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'subsystemJobListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
