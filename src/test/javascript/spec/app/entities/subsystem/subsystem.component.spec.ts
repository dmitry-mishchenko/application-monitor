/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemComponent } from '../../../../../../main/webapp/app/entities/subsystem/subsystem.component';
import { SubsystemService } from '../../../../../../main/webapp/app/entities/subsystem/subsystem.service';
import { Subsystem } from '../../../../../../main/webapp/app/entities/subsystem/subsystem.model';

describe('Component Tests', () => {

    describe('Subsystem Management Component', () => {
        let comp: SubsystemComponent;
        let fixture: ComponentFixture<SubsystemComponent>;
        let service: SubsystemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemComponent],
                providers: [
                    SubsystemService
                ]
            })
            .overrideTemplate(SubsystemComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Subsystem(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.subsystems[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
