/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemDetailComponent } from '../../../../../../main/webapp/app/entities/subsystem/subsystem-detail.component';
import { SubsystemService } from '../../../../../../main/webapp/app/entities/subsystem/subsystem.service';
import { Subsystem } from '../../../../../../main/webapp/app/entities/subsystem/subsystem.model';

describe('Component Tests', () => {

    describe('Subsystem Management Detail Component', () => {
        let comp: SubsystemDetailComponent;
        let fixture: ComponentFixture<SubsystemDetailComponent>;
        let service: SubsystemService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemDetailComponent],
                providers: [
                    SubsystemService
                ]
            })
            .overrideTemplate(SubsystemDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Subsystem(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.subsystem).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
