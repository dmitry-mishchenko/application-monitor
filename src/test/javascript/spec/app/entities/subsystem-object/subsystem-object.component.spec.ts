/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemObjectComponent } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object.component';
import { SubsystemObjectService } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object.service';
import { SubsystemObject } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object.model';

describe('Component Tests', () => {

    describe('SubsystemObject Management Component', () => {
        let comp: SubsystemObjectComponent;
        let fixture: ComponentFixture<SubsystemObjectComponent>;
        let service: SubsystemObjectService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemObjectComponent],
                providers: [
                    SubsystemObjectService
                ]
            })
            .overrideTemplate(SubsystemObjectComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemObjectComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemObjectService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SubsystemObject(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.subsystemObjects[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
