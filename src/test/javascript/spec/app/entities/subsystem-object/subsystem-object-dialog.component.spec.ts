/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemObjectDialogComponent } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object-dialog.component';
import { SubsystemObjectService } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object.service';
import { SubsystemObject } from '../../../../../../main/webapp/app/entities/subsystem-object/subsystem-object.model';
import { SubsystemService } from '../../../../../../main/webapp/app/entities/subsystem';
import { ObjectTypeService } from '../../../../../../main/webapp/app/entities/object-type';

describe('Component Tests', () => {

    describe('SubsystemObject Management Dialog Component', () => {
        let comp: SubsystemObjectDialogComponent;
        let fixture: ComponentFixture<SubsystemObjectDialogComponent>;
        let service: SubsystemObjectService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemObjectDialogComponent],
                providers: [
                    SubsystemService,
                    ObjectTypeService,
                    SubsystemObjectService
                ]
            })
            .overrideTemplate(SubsystemObjectDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemObjectDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemObjectService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SubsystemObject(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.subsystemObject = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'subsystemObjectListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SubsystemObject();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.subsystemObject = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'subsystemObjectListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
