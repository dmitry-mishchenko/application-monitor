/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemParameterDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter-delete-dialog.component';
import { SubsystemParameterService } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter.service';

describe('Component Tests', () => {

    describe('SubsystemParameter Management Delete Component', () => {
        let comp: SubsystemParameterDeleteDialogComponent;
        let fixture: ComponentFixture<SubsystemParameterDeleteDialogComponent>;
        let service: SubsystemParameterService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemParameterDeleteDialogComponent],
                providers: [
                    SubsystemParameterService
                ]
            })
            .overrideTemplate(SubsystemParameterDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemParameterDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemParameterService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
