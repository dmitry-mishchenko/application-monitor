/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemParameterDialogComponent } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter-dialog.component';
import { SubsystemParameterService } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter.service';
import { SubsystemParameter } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter.model';
import { SubsystemService } from '../../../../../../main/webapp/app/entities/subsystem';

describe('Component Tests', () => {

    describe('SubsystemParameter Management Dialog Component', () => {
        let comp: SubsystemParameterDialogComponent;
        let fixture: ComponentFixture<SubsystemParameterDialogComponent>;
        let service: SubsystemParameterService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemParameterDialogComponent],
                providers: [
                    SubsystemService,
                    SubsystemParameterService
                ]
            })
            .overrideTemplate(SubsystemParameterDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemParameterDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemParameterService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SubsystemParameter(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.subsystemParameter = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'subsystemParameterListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SubsystemParameter();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.subsystemParameter = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'subsystemParameterListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
