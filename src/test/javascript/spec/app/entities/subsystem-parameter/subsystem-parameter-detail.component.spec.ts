/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AppmonitorTestModule } from '../../../test.module';
import { SubsystemParameterDetailComponent } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter-detail.component';
import { SubsystemParameterService } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter.service';
import { SubsystemParameter } from '../../../../../../main/webapp/app/entities/subsystem-parameter/subsystem-parameter.model';

describe('Component Tests', () => {

    describe('SubsystemParameter Management Detail Component', () => {
        let comp: SubsystemParameterDetailComponent;
        let fixture: ComponentFixture<SubsystemParameterDetailComponent>;
        let service: SubsystemParameterService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [AppmonitorTestModule],
                declarations: [SubsystemParameterDetailComponent],
                providers: [
                    SubsystemParameterService
                ]
            })
            .overrideTemplate(SubsystemParameterDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SubsystemParameterDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SubsystemParameterService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SubsystemParameter(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.subsystemParameter).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
