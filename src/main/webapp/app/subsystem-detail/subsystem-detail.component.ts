import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs/Subscription';
import { Subsystem, SubsystemService } from '../entities/subsystem';
import { SubsystemJob, SubsystemJobService, SubsystemJobType } from '../entities/subsystem-job';
import { Account, Principal } from '../shared';
import { SubsystemJobLogModalComponent } from './subsystem-job-log-modal/subsystem-job-log-modal.component';

@Component({
    selector: 'jhi-subsystem-detail',
    templateUrl: './subsystem-detail.component.html',
    styleUrls: [
        'subsystem-detail.css'
    ]

})
export class SubsystemDetailComponent implements OnInit {

    account: Account;
    modalRef: NgbModalRef;
    subscription: Subscription;
    subsystemId: number;
    subsystem: Subsystem;
    subsystemJobs: SubsystemJob[];
    imsConnectSubsystemJob: SubsystemJob;
    imsControlRegionSubsystemJob: SubsystemJob;
    isUpdating: boolean;

    constructor(
        private principal: Principal,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private subsystemService: SubsystemService,
        public subsystemJobService: SubsystemJobService
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.subsystemId = params['subsystemId'];
            this.loadSubsystem();
            this.loadSubsystemJobs();
        });
        this.principal.identity().then((account) => {
            this.account = account;
        });
    }

    loadSubsystem() {
        this.subsystemService.find(this.subsystemId).subscribe(
            (res: HttpResponse<Subsystem>) => {
                this.subsystem = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    loadSubsystemJobs() {
        this.subsystemJobService.findBySubsystemId(this.subsystemId).subscribe(
            (res: HttpResponse<SubsystemJob[]>) => {
                this.imsConnectSubsystemJob = res.body.find((subsystemJob) => subsystemJob.type === SubsystemJobType.IMS_CONNECT);
                this.imsControlRegionSubsystemJob = res.body.find((subsystemJob) => subsystemJob.type === SubsystemJobType.IMS_CONTROL_REGION);
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        console.log(this.imsConnectSubsystemJob);
        console.log(this.imsControlRegionSubsystemJob);
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    private getSubsystemJobStartedAt(subsystemJob: SubsystemJob) {
        return (new Date(subsystemJob.startedAt.toString().substring(0, 19)).getTime());
    }

    private openSubsystemJobLogModal(subsystemJob: SubsystemJob) {
        const modal = this.modalService.open(SubsystemJobLogModalComponent, { size: 'lg' });
        modal.componentInstance.subsystemJob = subsystemJob;
    }

    private updateInformation(subsystemId: number) {
        this.isUpdating = false;
        this.subsystemService.updateInformation(subsystemId).subscribe(
            () => {
                this.loadSubsystem();
                this.loadSubsystemJobs();
                this.isUpdating = true;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
}
