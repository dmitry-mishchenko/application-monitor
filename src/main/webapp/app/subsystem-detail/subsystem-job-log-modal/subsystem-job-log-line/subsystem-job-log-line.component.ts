import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'jhi-subsystem-job-log-line',
    templateUrl: './subsystem-job-log-line.component.html',
    styleUrls: ['./subsystem-job-log-line.css']
})
export class SubsystemJobLogLineComponent implements OnInit {

    @Input()
    logLine: string;

    messageType: string;
    messageCode: string;
    infoMessageRegex: RegExp;
    errorMessageRegex: RegExp;
    warningMessageRegex: RegExp;

    constructor() {
        this.messageType = 'text';
        this.messageCode = '';
        this.infoMessageRegex = new RegExp('^(DFS|HWS|CSQ|DSN|EQQ).*(I)$');
        this.errorMessageRegex = new RegExp('^(DFS|HWS|CSQ|DSN|EQQ).*(A|E|S)$');
        this.warningMessageRegex = new RegExp('^(DFS|HWS|CSQ|DSN|EQQ).*(W)$');
    }

    ngOnInit() {
        this.logLine.replace(/\s+/g, ' ').trim().split(' ').forEach(
            (logLinePart) => {
                if (this.infoMessageRegex.test(logLinePart)) {
                    this.messageCode = logLinePart;
                    this.messageType = 'infoMessage';
                } else if (this.errorMessageRegex.test(logLinePart)) {
                    this.messageType = 'errorMessage';
                    this.messageCode = logLinePart;
                } else if (this.warningMessageRegex.test(logLinePart)) {
                    this.messageType = 'warningMessage';
                    this.messageCode = logLinePart;
                }
            });
    }

    findInKnowledgeCenter() {
        let href = '';
        if (this.messageCode.startsWith('DFS')) {
            href = `https://www.ibm.com/support/knowledgecenter/en/SSEPH2_13.1.0/com.ibm.ims13.doc.msgs/msgs/${this.messageCode.toLowerCase()}.html`;
        } else if (this.messageCode.startsWith('DSN')) {
            href = `https://www.ibm.com/support/knowledgecenter/en/SSEPEK_10.0.0/msgs/src/tpc/${this.messageCode.toLowerCase()}.html`;
        } else if (this.messageCode.startsWith('+DFS')) {
            href = `https://www.ibm.com/support/knowledgecenter/en/SSEPEK_10.0.0/msgs/src/tpc/${this.messageCode.toLowerCase().substr(1)}.html`;
        }
        window.open(href, '_blank');
    }
}
