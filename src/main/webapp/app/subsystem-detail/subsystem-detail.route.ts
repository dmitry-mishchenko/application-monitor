import { Route } from '@angular/router';

import { SubsystemDetailComponent } from './';

export const SUBSYSTEM_DETAIL_ROUTE: Route = {
    path: 'subsystem/:subsystemId/info',
    component: SubsystemDetailComponent,
    data: {
        authorities: [],
        pageTitle: 'appmonitorApp.subsystem-detail.title'
    }
};
