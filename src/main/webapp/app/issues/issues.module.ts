import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppmonitorSharedModule } from '../shared';

import { ISSUES_ROUTE, IssuesComponent } from './';
import { IssueCardComponent } from './issue-card/issue-card.component';

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild([ISSUES_ROUTE])
    ],
    declarations: [
        IssuesComponent,
        IssueCardComponent
    ],
    entryComponents: [
        IssueCardComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorIssuesModule { }
