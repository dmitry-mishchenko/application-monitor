import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Issue } from '../../entities/issue/issue.model';
import { Principal } from '../../shared/auth/principal.service';

@Component({
    selector: 'jhi-issue-card',
    templateUrl: './issue-card.component.html',
    styleUrls: [
        '../issues.css'
    ]
})

export class IssueCardComponent implements OnInit, OnDestroy {

    @Input() issue: Issue;

    constructor(
        private principal: Principal,
        private modalService: NgbModal,
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    getIssueOccuredDate() {
        return (new Date(this.issue.occuredAt.toString().substring(0, 19)).getTime());
    }

    getIssueResolvedDate() {
        if (this.issue.resolvedAt != null) {
            return (new Date(this.issue.resolvedAt.toString().substring(0, 19)).getTime());
        } else {
            return null;
        }
    }
}
