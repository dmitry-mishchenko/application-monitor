import { Route } from '@angular/router';

import { IssuesComponent } from './';

export const ISSUES_ROUTE: Route = {
    path: 'subsystem/:subsystemId/issues',
    component: IssuesComponent,
    data: {
        authorities: [],
        pageTitle: 'appmonitorApp.issue.home.title'
    }
};
