import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppmonitorSharedModule } from '../shared';
import { ISSUE_DETAIL_ROUTE, IssueDetailComponent } from './';

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild([ISSUE_DETAIL_ROUTE])
    ],
    declarations: [
        IssueDetailComponent
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorIssueDetailModule { }
