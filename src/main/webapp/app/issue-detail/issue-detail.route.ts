import { Route } from '@angular/router';

import { IssueDetailComponent } from './';

export const ISSUE_DETAIL_ROUTE: Route = {
    path: 'subsystem/:subsystemId/issues/:issueId',
    component: IssueDetailComponent,
    data: {
        authorities: [],
        pageTitle: 'appmonitorApp.issue.detail.title'
    }
};
