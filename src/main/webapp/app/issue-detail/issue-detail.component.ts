import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Subscription } from 'rxjs/Subscription';
import { Issue, IssueService } from '../entities/issue';
import { Account, Principal } from '../shared';

@Component({
    selector: 'jhi-issue-detail',
    templateUrl: './issue-detail.component.html',
    styleUrls: [
        'issue-detail.css'
    ]
})
export class IssueDetailComponent implements OnInit {

    account: Account;
    modalRef: NgbModalRef;
    subscription: Subscription;
    issueId: number;
    issue: Issue;
    commands: string;
    response: string;

    constructor(
        private principal: Principal,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private route: ActivatedRoute,
        private modalService: NgbModal,
        private issueService: IssueService
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.issueId = params['issueId'];
            this.loadIssue();
            this.commands = '';
            this.response = '';
        });
        this.principal.identity().then((account) => {
            this.account = account;
        });
    }

    loadIssue() {
        this.issueService.find(this.issueId).subscribe(
            (res: HttpResponse<Issue>) => {
                this.issue = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    private execute() {
        console.log('Executing command: ' + this.commands);
        this.issueService.executeCommand(this.issue.id, this.commands).subscribe(
            (res: HttpResponse<string>) => {
                this.response = res.body;
            },
            (res: HttpErrorResponse) => {
                this.onError(res.message);
            }
        );
    }

    private getOccuredAtDate() {
        if (this.issue != null) {
            return (new Date(this.issue.occuredAt.toString().substring(0, 19)).getTime());
        } else {
            return undefined;
        }
    }

    private getResolvedAtDate() {
        if (this.issue != null) {
            return (new Date(this.issue.resolvedAt.toString().substring(0, 19)).getTime());
        } else {
            return undefined;
        }
    }

    private assignIssueToMe() {
        this.principal.identity().then((account) => {
            this.issue.assignedUserId = account.id;
            this.issueService.update(this.issue).subscribe((res) => {
                this.issue = res.body;
            });
        });
    }
}
