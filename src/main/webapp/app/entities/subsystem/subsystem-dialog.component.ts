import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { Subsystem } from './subsystem.model';
import { SubsystemPopupService } from './subsystem-popup.service';
import { SubsystemService } from './subsystem.service';

@Component({
    selector: 'jhi-subsystem-dialog',
    templateUrl: './subsystem-dialog.component.html'
})
export class SubsystemDialogComponent implements OnInit {

    subsystem: Subsystem;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private subsystemService: SubsystemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.subsystem.id !== undefined) {
            this.subscribeToSaveResponse(
                this.subsystemService.update(this.subsystem));
        } else {
            this.subscribeToSaveResponse(
                this.subsystemService.create(this.subsystem));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Subsystem>>) {
        result.subscribe((res: HttpResponse<Subsystem>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Subsystem) {
        this.eventManager.broadcast({ name: 'subsystemListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-subsystem-popup',
    template: ''
})
export class SubsystemPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subsystemPopupService: SubsystemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.subsystemPopupService
                    .open(SubsystemDialogComponent as Component, params['id']);
            } else {
                this.subsystemPopupService
                    .open(SubsystemDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
