import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Subsystem } from './subsystem.model';
import { SubsystemPopupService } from './subsystem-popup.service';
import { SubsystemService } from './subsystem.service';

@Component({
    selector: 'jhi-subsystem-delete-dialog',
    templateUrl: './subsystem-delete-dialog.component.html'
})
export class SubsystemDeleteDialogComponent {

    subsystem: Subsystem;

    constructor(
        private subsystemService: SubsystemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.subsystemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'subsystemListModification',
                content: 'Deleted an subsystem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-subsystem-delete-popup',
    template: ''
})
export class SubsystemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subsystemPopupService: SubsystemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.subsystemPopupService
                .open(SubsystemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
