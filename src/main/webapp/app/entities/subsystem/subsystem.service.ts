import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JhiDateUtils } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import { createRequestOption } from '../../shared';
import { Subsystem } from './subsystem.model';

export type EntityResponseType = HttpResponse<Subsystem>;

@Injectable()
export class SubsystemService {

    private resourceUrl = SERVER_API_URL + 'api/subsystems';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(subsystem: Subsystem): Observable<EntityResponseType> {
        const copy = this.convert(subsystem);
        return this.http.post<Subsystem>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(subsystem: Subsystem): Observable<EntityResponseType> {
        const copy = this.convert(subsystem);
        return this.http.put<Subsystem>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Subsystem>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Subsystem[]>> {
        const options = createRequestOption(req);
        return this.http.get<Subsystem[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Subsystem[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    updateIssues(id: number): Observable<HttpResponse<any>> {
        return this.http.get(`${this.resourceUrl}/updateIssues/${id}`, { observe: 'response' });
    }

    updateInformation(id: number): Observable<HttpResponse<any>> {
        return this.http.get(`${this.resourceUrl}/updateInformation/${id}`, { observe: 'response' });
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Subsystem = this.convertItemFromServer(res.body);
        return res.clone({ body });
    }

    private convertArrayResponse(res: HttpResponse<Subsystem[]>): HttpResponse<Subsystem[]> {
        const jsonResponse: Subsystem[] = res.body;
        const body: Subsystem[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({ body });
    }

    /**
     * Convert a returned JSON object to Subsystem.
     */
    private convertItemFromServer(subsystem: Subsystem): Subsystem {
        const copy: Subsystem = Object.assign({}, subsystem);
        copy.updatedAt = this.dateUtils
            .convertDateTimeFromServer(subsystem.updatedAt);
        return copy;
    }

    /**
     * Convert a Subsystem to a JSON which can be sent to the server.
     */
    private convert(subsystem: Subsystem): Subsystem {
        const copy: Subsystem = Object.assign({}, subsystem);

        copy.updatedAt = this.dateUtils.toDate(subsystem.updatedAt);
        return copy;
    }
}
