import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SubsystemComponent } from './subsystem.component';
import { SubsystemDetailComponent } from './subsystem-detail.component';
import { SubsystemPopupComponent } from './subsystem-dialog.component';
import { SubsystemDeletePopupComponent } from './subsystem-delete-dialog.component';

export const subsystemRoute: Routes = [
    {
        path: 'subsystem',
        component: SubsystemComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'subsystem/:id',
        component: SubsystemDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystem.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const subsystemPopupRoute: Routes = [
    {
        path: 'subsystem-new',
        component: SubsystemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subsystem/:id/edit',
        component: SubsystemPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subsystem/:id/delete',
        component: SubsystemDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystem.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
