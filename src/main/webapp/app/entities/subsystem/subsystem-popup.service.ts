import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { Subsystem } from './subsystem.model';
import { SubsystemService } from './subsystem.service';

@Injectable()
export class SubsystemPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private subsystemService: SubsystemService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.subsystemService.find(id)
                    .subscribe((subsystemResponse: HttpResponse<Subsystem>) => {
                        const subsystem: Subsystem = subsystemResponse.body;
                        subsystem.updatedAt = this.datePipe
                            .transform(subsystem.updatedAt, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.subsystemModalRef(component, subsystem);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.subsystemModalRef(component, new Subsystem());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    subsystemModalRef(component: Component, subsystem: Subsystem): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.subsystem = subsystem;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
