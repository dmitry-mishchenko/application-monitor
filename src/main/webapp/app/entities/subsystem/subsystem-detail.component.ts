import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { Subsystem } from './subsystem.model';
import { SubsystemService } from './subsystem.service';

@Component({
    selector: 'jhi-subsystem-detail',
    templateUrl: './subsystem-detail.component.html'
})
export class SubsystemDetailComponent implements OnInit, OnDestroy {

    subsystem: Subsystem;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private subsystemService: SubsystemService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSubsystems();
    }

    load(id) {
        this.subsystemService.find(id)
            .subscribe((subsystemResponse: HttpResponse<Subsystem>) => {
                this.subsystem = subsystemResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSubsystems() {
        this.eventSubscriber = this.eventManager.subscribe(
            'subsystemListModification',
            (response) => this.load(this.subsystem.id)
        );
    }
}
