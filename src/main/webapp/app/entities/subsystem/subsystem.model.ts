import { BaseEntity } from './../../shared';

export const enum SubsystemType {
    'IMS',
    'MQ',
    'OPC',
    'DB2'
}

export class Subsystem implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public subsystemType?: SubsystemType,
        public refreshRate?: number,
        public updatedAt?: any,
        public information?: any,
    ) {
    }
}
