import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { SubsystemJob } from './subsystem-job.model';
import { SubsystemJobService } from './subsystem-job.service';

@Injectable()
export class SubsystemJobPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private subsystemJobService: SubsystemJobService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.subsystemJobService.find(id)
                    .subscribe((subsystemJobResponse: HttpResponse<SubsystemJob>) => {
                        const subsystemJob: SubsystemJob = subsystemJobResponse.body;
                        subsystemJob.startedAt = this.datePipe
                            .transform(subsystemJob.startedAt, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.subsystemJobModalRef(component, subsystemJob);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.subsystemJobModalRef(component, new SubsystemJob());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    subsystemJobModalRef(component: Component, subsystemJob: SubsystemJob): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.subsystemJob = subsystemJob;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
