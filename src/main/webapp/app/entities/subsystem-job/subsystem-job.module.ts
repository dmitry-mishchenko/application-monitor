import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppmonitorSharedModule } from '../../shared';
import {
    SubsystemJobService,
    SubsystemJobPopupService,
    SubsystemJobComponent,
    SubsystemJobDetailComponent,
    SubsystemJobDialogComponent,
    SubsystemJobPopupComponent,
    SubsystemJobDeletePopupComponent,
    SubsystemJobDeleteDialogComponent,
    subsystemJobRoute,
    subsystemJobPopupRoute,
    SubsystemJobResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...subsystemJobRoute,
    ...subsystemJobPopupRoute,
];

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SubsystemJobComponent,
        SubsystemJobDetailComponent,
        SubsystemJobDialogComponent,
        SubsystemJobDeleteDialogComponent,
        SubsystemJobPopupComponent,
        SubsystemJobDeletePopupComponent,
    ],
    entryComponents: [
        SubsystemJobComponent,
        SubsystemJobDialogComponent,
        SubsystemJobPopupComponent,
        SubsystemJobDeleteDialogComponent,
        SubsystemJobDeletePopupComponent,
    ],
    providers: [
        SubsystemJobService,
        SubsystemJobPopupService,
        SubsystemJobResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorSubsystemJobModule {}
