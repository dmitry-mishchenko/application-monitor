import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SubsystemJob } from './subsystem-job.model';
import { SubsystemJobPopupService } from './subsystem-job-popup.service';
import { SubsystemJobService } from './subsystem-job.service';

@Component({
    selector: 'jhi-subsystem-job-delete-dialog',
    templateUrl: './subsystem-job-delete-dialog.component.html'
})
export class SubsystemJobDeleteDialogComponent {

    subsystemJob: SubsystemJob;

    constructor(
        private subsystemJobService: SubsystemJobService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.subsystemJobService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'subsystemJobListModification',
                content: 'Deleted an subsystemJob'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-subsystem-job-delete-popup',
    template: ''
})
export class SubsystemJobDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subsystemJobPopupService: SubsystemJobPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.subsystemJobPopupService
                .open(SubsystemJobDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
