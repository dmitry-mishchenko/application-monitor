import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JhiDateUtils } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import { createRequestOption } from '../../shared';
import { SubsystemJob } from './subsystem-job.model';

export type EntityResponseType = HttpResponse<SubsystemJob>;

@Injectable()
export class SubsystemJobService {

    private resourceUrl = SERVER_API_URL + 'api/subsystem-jobs';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(subsystemJob: SubsystemJob): Observable<EntityResponseType> {
        const copy = this.convert(subsystemJob);
        return this.http.post<SubsystemJob>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(subsystemJob: SubsystemJob): Observable<EntityResponseType> {
        const copy = this.convert(subsystemJob);
        return this.http.put<SubsystemJob>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SubsystemJob>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    findBySubsystemId(id: number): Observable<HttpResponse<SubsystemJob[]>> {
        return this.http.get<SubsystemJob[]>(`${this.resourceUrl}/getBySubsystem/${id}`, { observe: 'response' })
            .map((res: HttpResponse<SubsystemJob[]>) => this.convertArrayResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SubsystemJob[]>> {
        const options = createRequestOption(req);
        return this.http.get<SubsystemJob[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SubsystemJob[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SubsystemJob = this.convertItemFromServer(res.body);
        return res.clone({ body });
    }

    private convertArrayResponse(res: HttpResponse<SubsystemJob[]>): HttpResponse<SubsystemJob[]> {
        const jsonResponse: SubsystemJob[] = res.body;
        const body: SubsystemJob[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({ body });
    }

    /**
     * Convert a returned JSON object to SubsystemJob.
     */
    private convertItemFromServer(subsystemJob: SubsystemJob): SubsystemJob {
        const copy: SubsystemJob = Object.assign({}, subsystemJob);
        copy.startedAt = this.dateUtils
            .convertDateTimeFromServer(subsystemJob.startedAt);
        return copy;
    }

    /**
     * Convert a SubsystemJob to a JSON which can be sent to the server.
     */
    private convert(subsystemJob: SubsystemJob): SubsystemJob {
        const copy: SubsystemJob = Object.assign({}, subsystemJob);

        copy.startedAt = this.dateUtils.toDate(subsystemJob.startedAt);
        return copy;
    }
}
