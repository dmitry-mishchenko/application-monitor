import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SubsystemJobComponent } from './subsystem-job.component';
import { SubsystemJobDetailComponent } from './subsystem-job-detail.component';
import { SubsystemJobPopupComponent } from './subsystem-job-dialog.component';
import { SubsystemJobDeletePopupComponent } from './subsystem-job-delete-dialog.component';

@Injectable()
export class SubsystemJobResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const subsystemJobRoute: Routes = [
    {
        path: 'subsystem-job',
        component: SubsystemJobComponent,
        resolve: {
            'pagingParams': SubsystemJobResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemJob.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'subsystem-job/:id',
        component: SubsystemJobDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemJob.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const subsystemJobPopupRoute: Routes = [
    {
        path: 'subsystem-job-new',
        component: SubsystemJobPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemJob.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subsystem-job/:id/edit',
        component: SubsystemJobPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemJob.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'subsystem-job/:id/delete',
        component: SubsystemJobDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.subsystemJob.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
