import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { SubsystemJob } from './subsystem-job.model';
import { SubsystemJobService } from './subsystem-job.service';

@Component({
    selector: 'jhi-subsystem-job-detail',
    templateUrl: './subsystem-job-detail.component.html'
})
export class SubsystemJobDetailComponent implements OnInit, OnDestroy {

    subsystemJob: SubsystemJob;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private subsystemJobService: SubsystemJobService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSubsystemJobs();
    }

    load(id) {
        this.subsystemJobService.find(id)
            .subscribe((subsystemJobResponse: HttpResponse<SubsystemJob>) => {
                this.subsystemJob = subsystemJobResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSubsystemJobs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'subsystemJobListModification',
            (response) => this.load(this.subsystemJob.id)
        );
    }
}
