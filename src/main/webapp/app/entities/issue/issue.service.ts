import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JhiDateUtils } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';
import { createRequestOption } from '../../shared';
import { Issue } from './issue.model';

export type EntityResponseType = HttpResponse<Issue>;

@Injectable()
export class IssueService {

    private resourceUrl = SERVER_API_URL + 'api/issues';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(issue: Issue): Observable<EntityResponseType> {
        const copy = this.convert(issue);
        return this.http.post<Issue>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(issue: Issue): Observable<EntityResponseType> {
        const copy = this.convert(issue);
        return this.http.put<Issue>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Issue>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    findBySubsystemId(id: number): Observable<HttpResponse<Issue[]>> {
        return this.http.get<Issue[]>(`${this.resourceUrl}/getBySubsystem/${id}`, { observe: 'response' })
            .map((res: HttpResponse<Issue[]>) => this.convertArrayResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Issue[]>> {
        const options = createRequestOption(req);
        return this.http.get<Issue[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Issue[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    executeCommand(id: number, command: String): Observable<HttpResponse<string>> {
        return this.http.post(`${this.resourceUrl}/${id}/executeCommand`, command, { observe: 'response', responseType: 'text' })
            .map((res: HttpResponse<string>) => res);
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Issue = this.convertItemFromServer(res.body);
        return res.clone({ body });
    }

    private convertArrayResponse(res: HttpResponse<Issue[]>): HttpResponse<Issue[]> {
        const jsonResponse: Issue[] = res.body;
        const body: Issue[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({ body });
    }

    /**
     * Convert a returned JSON object to Issue.
     */
    private convertItemFromServer(issue: Issue): Issue {
        const copy: Issue = Object.assign({}, issue);
        copy.occuredAt = this.dateUtils
            .convertDateTimeFromServer(issue.occuredAt);
        copy.resolvedAt = this.dateUtils
            .convertDateTimeFromServer(issue.resolvedAt);
        return copy;
    }

    /**
     * Convert a Issue to a JSON which can be sent to the server.
     */
    private convert(issue: Issue): Issue {
        const copy: Issue = Object.assign({}, issue);
        copy.occuredAt = issue.occuredAt;
        copy.resolvedAt = issue.resolvedAt;
        return copy;
    }
}
