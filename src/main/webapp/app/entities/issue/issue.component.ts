import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Issue } from './issue.model';
import { IssueService } from './issue.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-issue',
    templateUrl: './issue.component.html'
})
export class IssueComponent implements OnInit, OnDestroy {
issues: Issue[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private issueService: IssueService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.issueService.query().subscribe(
            (res: HttpResponse<Issue[]>) => {
                this.issues = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInIssues();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Issue) {
        return item.id;
    }
    registerChangeInIssues() {
        this.eventSubscriber = this.eventManager.subscribe('issueListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
