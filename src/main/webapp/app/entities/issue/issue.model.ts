import { BaseEntity } from './../../shared';

export class Issue implements BaseEntity {
    constructor(
        public id?: number,
        public occuredAt?: any,
        public resolvedAt?: any,
        public objectStatusId?: number,
        public subsystemObjectId?: number,
        public assignedUserLogin?: string,
        public assignedUserId?: number,
    ) {
    }
}
