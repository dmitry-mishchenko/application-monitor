import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { SubsystemParameter } from './subsystem-parameter.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SubsystemParameter>;

@Injectable()
export class SubsystemParameterService {

    private resourceUrl =  SERVER_API_URL + 'api/subsystem-parameters';

    constructor(private http: HttpClient) { }

    create(subsystemParameter: SubsystemParameter): Observable<EntityResponseType> {
        const copy = this.convert(subsystemParameter);
        return this.http.post<SubsystemParameter>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(subsystemParameter: SubsystemParameter): Observable<EntityResponseType> {
        const copy = this.convert(subsystemParameter);
        return this.http.put<SubsystemParameter>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SubsystemParameter>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SubsystemParameter[]>> {
        const options = createRequestOption(req);
        return this.http.get<SubsystemParameter[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SubsystemParameter[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SubsystemParameter = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SubsystemParameter[]>): HttpResponse<SubsystemParameter[]> {
        const jsonResponse: SubsystemParameter[] = res.body;
        const body: SubsystemParameter[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SubsystemParameter.
     */
    private convertItemFromServer(subsystemParameter: SubsystemParameter): SubsystemParameter {
        const copy: SubsystemParameter = Object.assign({}, subsystemParameter);
        return copy;
    }

    /**
     * Convert a SubsystemParameter to a JSON which can be sent to the server.
     */
    private convert(subsystemParameter: SubsystemParameter): SubsystemParameter {
        const copy: SubsystemParameter = Object.assign({}, subsystemParameter);
        return copy;
    }
}
