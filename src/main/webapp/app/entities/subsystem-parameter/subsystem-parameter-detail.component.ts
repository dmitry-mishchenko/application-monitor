import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SubsystemParameter } from './subsystem-parameter.model';
import { SubsystemParameterService } from './subsystem-parameter.service';

@Component({
    selector: 'jhi-subsystem-parameter-detail',
    templateUrl: './subsystem-parameter-detail.component.html'
})
export class SubsystemParameterDetailComponent implements OnInit, OnDestroy {

    subsystemParameter: SubsystemParameter;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private subsystemParameterService: SubsystemParameterService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSubsystemParameters();
    }

    load(id) {
        this.subsystemParameterService.find(id)
            .subscribe((subsystemParameterResponse: HttpResponse<SubsystemParameter>) => {
                this.subsystemParameter = subsystemParameterResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSubsystemParameters() {
        this.eventSubscriber = this.eventManager.subscribe(
            'subsystemParameterListModification',
            (response) => this.load(this.subsystemParameter.id)
        );
    }
}
