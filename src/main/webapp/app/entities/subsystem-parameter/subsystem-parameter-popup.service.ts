import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SubsystemParameter } from './subsystem-parameter.model';
import { SubsystemParameterService } from './subsystem-parameter.service';

@Injectable()
export class SubsystemParameterPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private subsystemParameterService: SubsystemParameterService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.subsystemParameterService.find(id)
                    .subscribe((subsystemParameterResponse: HttpResponse<SubsystemParameter>) => {
                        const subsystemParameter: SubsystemParameter = subsystemParameterResponse.body;
                        this.ngbModalRef = this.subsystemParameterModalRef(component, subsystemParameter);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.subsystemParameterModalRef(component, new SubsystemParameter());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    subsystemParameterModalRef(component: Component, subsystemParameter: SubsystemParameter): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.subsystemParameter = subsystemParameter;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
