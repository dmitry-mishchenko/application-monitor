import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SubsystemParameter } from './subsystem-parameter.model';
import { SubsystemParameterService } from './subsystem-parameter.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-subsystem-parameter',
    templateUrl: './subsystem-parameter.component.html'
})
export class SubsystemParameterComponent implements OnInit, OnDestroy {
subsystemParameters: SubsystemParameter[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private subsystemParameterService: SubsystemParameterService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.subsystemParameterService.query().subscribe(
            (res: HttpResponse<SubsystemParameter[]>) => {
                this.subsystemParameters = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSubsystemParameters();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SubsystemParameter) {
        return item.id;
    }
    registerChangeInSubsystemParameters() {
        this.eventSubscriber = this.eventManager.subscribe('subsystemParameterListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
