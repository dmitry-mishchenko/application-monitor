import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppmonitorSharedModule } from '../../shared';
import {
    SubsystemParameterService,
    SubsystemParameterPopupService,
    SubsystemParameterComponent,
    SubsystemParameterDetailComponent,
    SubsystemParameterDialogComponent,
    SubsystemParameterPopupComponent,
    SubsystemParameterDeletePopupComponent,
    SubsystemParameterDeleteDialogComponent,
    subsystemParameterRoute,
    subsystemParameterPopupRoute,
} from './';

const ENTITY_STATES = [
    ...subsystemParameterRoute,
    ...subsystemParameterPopupRoute,
];

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SubsystemParameterComponent,
        SubsystemParameterDetailComponent,
        SubsystemParameterDialogComponent,
        SubsystemParameterDeleteDialogComponent,
        SubsystemParameterPopupComponent,
        SubsystemParameterDeletePopupComponent,
    ],
    entryComponents: [
        SubsystemParameterComponent,
        SubsystemParameterDialogComponent,
        SubsystemParameterPopupComponent,
        SubsystemParameterDeleteDialogComponent,
        SubsystemParameterDeletePopupComponent,
    ],
    providers: [
        SubsystemParameterService,
        SubsystemParameterPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorSubsystemParameterModule {}
