import { BaseEntity } from './../../shared';

export class SubsystemParameter implements BaseEntity {
    constructor(
        public id?: number,
        public parameterKey?: string,
        public parameterValue?: string,
        public subsystemId?: number,
    ) {
    }
}
