import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SubsystemParameter } from './subsystem-parameter.model';
import { SubsystemParameterPopupService } from './subsystem-parameter-popup.service';
import { SubsystemParameterService } from './subsystem-parameter.service';
import { Subsystem, SubsystemService } from '../subsystem';

@Component({
    selector: 'jhi-subsystem-parameter-dialog',
    templateUrl: './subsystem-parameter-dialog.component.html'
})
export class SubsystemParameterDialogComponent implements OnInit {

    subsystemParameter: SubsystemParameter;
    isSaving: boolean;

    subsystems: Subsystem[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private subsystemParameterService: SubsystemParameterService,
        private subsystemService: SubsystemService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.subsystemService.query()
            .subscribe((res: HttpResponse<Subsystem[]>) => { this.subsystems = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.subsystemParameter.id !== undefined) {
            this.subscribeToSaveResponse(
                this.subsystemParameterService.update(this.subsystemParameter));
        } else {
            this.subscribeToSaveResponse(
                this.subsystemParameterService.create(this.subsystemParameter));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SubsystemParameter>>) {
        result.subscribe((res: HttpResponse<SubsystemParameter>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SubsystemParameter) {
        this.eventManager.broadcast({ name: 'subsystemParameterListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSubsystemById(index: number, item: Subsystem) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-subsystem-parameter-popup',
    template: ''
})
export class SubsystemParameterPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subsystemParameterPopupService: SubsystemParameterPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.subsystemParameterPopupService
                    .open(SubsystemParameterDialogComponent as Component, params['id']);
            } else {
                this.subsystemParameterPopupService
                    .open(SubsystemParameterDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
