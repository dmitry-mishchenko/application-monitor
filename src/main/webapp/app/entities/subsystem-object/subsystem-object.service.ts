import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { SubsystemObject } from './subsystem-object.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SubsystemObject>;

@Injectable()
export class SubsystemObjectService {

    private resourceUrl =  SERVER_API_URL + 'api/subsystem-objects';

    constructor(private http: HttpClient) { }

    create(subsystemObject: SubsystemObject): Observable<EntityResponseType> {
        const copy = this.convert(subsystemObject);
        return this.http.post<SubsystemObject>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(subsystemObject: SubsystemObject): Observable<EntityResponseType> {
        const copy = this.convert(subsystemObject);
        return this.http.put<SubsystemObject>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<SubsystemObject>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SubsystemObject[]>> {
        const options = createRequestOption(req);
        return this.http.get<SubsystemObject[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SubsystemObject[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SubsystemObject = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SubsystemObject[]>): HttpResponse<SubsystemObject[]> {
        const jsonResponse: SubsystemObject[] = res.body;
        const body: SubsystemObject[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SubsystemObject.
     */
    private convertItemFromServer(subsystemObject: SubsystemObject): SubsystemObject {
        const copy: SubsystemObject = Object.assign({}, subsystemObject);
        return copy;
    }

    /**
     * Convert a SubsystemObject to a JSON which can be sent to the server.
     */
    private convert(subsystemObject: SubsystemObject): SubsystemObject {
        const copy: SubsystemObject = Object.assign({}, subsystemObject);
        return copy;
    }
}
