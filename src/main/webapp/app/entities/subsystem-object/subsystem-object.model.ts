import { BaseEntity } from './../../shared';

export class SubsystemObject implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public isActive?: boolean,
        public subsystemId?: number,
        public objectTypeId?: number,
    ) {
        this.isActive = false;
    }
}
