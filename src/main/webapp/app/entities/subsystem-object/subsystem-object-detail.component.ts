import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SubsystemObject } from './subsystem-object.model';
import { SubsystemObjectService } from './subsystem-object.service';

@Component({
    selector: 'jhi-subsystem-object-detail',
    templateUrl: './subsystem-object-detail.component.html'
})
export class SubsystemObjectDetailComponent implements OnInit, OnDestroy {

    subsystemObject: SubsystemObject;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private subsystemObjectService: SubsystemObjectService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSubsystemObjects();
    }

    load(id) {
        this.subsystemObjectService.find(id)
            .subscribe((subsystemObjectResponse: HttpResponse<SubsystemObject>) => {
                this.subsystemObject = subsystemObjectResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSubsystemObjects() {
        this.eventSubscriber = this.eventManager.subscribe(
            'subsystemObjectListModification',
            (response) => this.load(this.subsystemObject.id)
        );
    }
}
