import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SubsystemObject } from './subsystem-object.model';
import { SubsystemObjectPopupService } from './subsystem-object-popup.service';
import { SubsystemObjectService } from './subsystem-object.service';

@Component({
    selector: 'jhi-subsystem-object-delete-dialog',
    templateUrl: './subsystem-object-delete-dialog.component.html'
})
export class SubsystemObjectDeleteDialogComponent {

    subsystemObject: SubsystemObject;

    constructor(
        private subsystemObjectService: SubsystemObjectService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.subsystemObjectService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'subsystemObjectListModification',
                content: 'Deleted an subsystemObject'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-subsystem-object-delete-popup',
    template: ''
})
export class SubsystemObjectDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subsystemObjectPopupService: SubsystemObjectPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.subsystemObjectPopupService
                .open(SubsystemObjectDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
