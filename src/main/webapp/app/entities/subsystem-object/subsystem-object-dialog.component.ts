import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SubsystemObject } from './subsystem-object.model';
import { SubsystemObjectPopupService } from './subsystem-object-popup.service';
import { SubsystemObjectService } from './subsystem-object.service';
import { Subsystem, SubsystemService } from '../subsystem';
import { ObjectType, ObjectTypeService } from '../object-type';

@Component({
    selector: 'jhi-subsystem-object-dialog',
    templateUrl: './subsystem-object-dialog.component.html'
})
export class SubsystemObjectDialogComponent implements OnInit {

    subsystemObject: SubsystemObject;
    isSaving: boolean;

    subsystems: Subsystem[];

    objecttypes: ObjectType[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private subsystemObjectService: SubsystemObjectService,
        private subsystemService: SubsystemService,
        private objectTypeService: ObjectTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.subsystemService.query()
            .subscribe((res: HttpResponse<Subsystem[]>) => { this.subsystems = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.objectTypeService.query()
            .subscribe((res: HttpResponse<ObjectType[]>) => { this.objecttypes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.subsystemObject.id !== undefined) {
            this.subscribeToSaveResponse(
                this.subsystemObjectService.update(this.subsystemObject));
        } else {
            this.subscribeToSaveResponse(
                this.subsystemObjectService.create(this.subsystemObject));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SubsystemObject>>) {
        result.subscribe((res: HttpResponse<SubsystemObject>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SubsystemObject) {
        this.eventManager.broadcast({ name: 'subsystemObjectListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackSubsystemById(index: number, item: Subsystem) {
        return item.id;
    }

    trackObjectTypeById(index: number, item: ObjectType) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-subsystem-object-popup',
    template: ''
})
export class SubsystemObjectPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private subsystemObjectPopupService: SubsystemObjectPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.subsystemObjectPopupService
                    .open(SubsystemObjectDialogComponent as Component, params['id']);
            } else {
                this.subsystemObjectPopupService
                    .open(SubsystemObjectDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
