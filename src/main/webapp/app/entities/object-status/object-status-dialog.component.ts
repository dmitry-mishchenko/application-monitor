import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ObjectStatus } from './object-status.model';
import { ObjectStatusPopupService } from './object-status-popup.service';
import { ObjectStatusService } from './object-status.service';
import { ObjectType, ObjectTypeService } from '../object-type';

@Component({
    selector: 'jhi-object-status-dialog',
    templateUrl: './object-status-dialog.component.html'
})
export class ObjectStatusDialogComponent implements OnInit {

    objectStatus: ObjectStatus;
    isSaving: boolean;

    objecttypes: ObjectType[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private objectStatusService: ObjectStatusService,
        private objectTypeService: ObjectTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.objectTypeService.query()
            .subscribe((res: HttpResponse<ObjectType[]>) => { this.objecttypes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.objectStatus.id !== undefined) {
            this.subscribeToSaveResponse(
                this.objectStatusService.update(this.objectStatus));
        } else {
            this.subscribeToSaveResponse(
                this.objectStatusService.create(this.objectStatus));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ObjectStatus>>) {
        result.subscribe((res: HttpResponse<ObjectStatus>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ObjectStatus) {
        this.eventManager.broadcast({ name: 'objectStatusListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackObjectTypeById(index: number, item: ObjectType) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-object-status-popup',
    template: ''
})
export class ObjectStatusPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private objectStatusPopupService: ObjectStatusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.objectStatusPopupService
                    .open(ObjectStatusDialogComponent as Component, params['id']);
            } else {
                this.objectStatusPopupService
                    .open(ObjectStatusDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
