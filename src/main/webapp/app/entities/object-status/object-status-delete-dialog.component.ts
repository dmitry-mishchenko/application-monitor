import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ObjectStatus } from './object-status.model';
import { ObjectStatusPopupService } from './object-status-popup.service';
import { ObjectStatusService } from './object-status.service';

@Component({
    selector: 'jhi-object-status-delete-dialog',
    templateUrl: './object-status-delete-dialog.component.html'
})
export class ObjectStatusDeleteDialogComponent {

    objectStatus: ObjectStatus;

    constructor(
        private objectStatusService: ObjectStatusService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.objectStatusService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'objectStatusListModification',
                content: 'Deleted an objectStatus'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-object-status-delete-popup',
    template: ''
})
export class ObjectStatusDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private objectStatusPopupService: ObjectStatusPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.objectStatusPopupService
                .open(ObjectStatusDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
