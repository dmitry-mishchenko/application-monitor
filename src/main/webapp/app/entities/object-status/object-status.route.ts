import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ObjectStatusComponent } from './object-status.component';
import { ObjectStatusDetailComponent } from './object-status-detail.component';
import { ObjectStatusPopupComponent } from './object-status-dialog.component';
import { ObjectStatusDeletePopupComponent } from './object-status-delete-dialog.component';

export const objectStatusRoute: Routes = [
    {
        path: 'object-status',
        component: ObjectStatusComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'object-status/:id',
        component: ObjectStatusDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectStatus.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const objectStatusPopupRoute: Routes = [
    {
        path: 'object-status-new',
        component: ObjectStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'object-status/:id/edit',
        component: ObjectStatusPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'object-status/:id/delete',
        component: ObjectStatusDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectStatus.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
