import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ObjectType } from './object-type.model';
import { ObjectTypeService } from './object-type.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-object-type',
    templateUrl: './object-type.component.html'
})
export class ObjectTypeComponent implements OnInit, OnDestroy {
objectTypes: ObjectType[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private objectTypeService: ObjectTypeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.objectTypeService.query().subscribe(
            (res: HttpResponse<ObjectType[]>) => {
                this.objectTypes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInObjectTypes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ObjectType) {
        return item.id;
    }
    registerChangeInObjectTypes() {
        this.eventSubscriber = this.eventManager.subscribe('objectTypeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
