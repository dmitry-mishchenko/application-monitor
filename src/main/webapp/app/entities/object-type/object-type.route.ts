import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ObjectTypeComponent } from './object-type.component';
import { ObjectTypeDetailComponent } from './object-type-detail.component';
import { ObjectTypePopupComponent } from './object-type-dialog.component';
import { ObjectTypeDeletePopupComponent } from './object-type-delete-dialog.component';

export const objectTypeRoute: Routes = [
    {
        path: 'object-type',
        component: ObjectTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'object-type/:id',
        component: ObjectTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const objectTypePopupRoute: Routes = [
    {
        path: 'object-type-new',
        component: ObjectTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'object-type/:id/edit',
        component: ObjectTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'object-type/:id/delete',
        component: ObjectTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'appmonitorApp.objectType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
