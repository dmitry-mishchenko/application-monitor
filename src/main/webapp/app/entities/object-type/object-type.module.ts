import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppmonitorSharedModule } from '../../shared';
import {
    ObjectTypeService,
    ObjectTypePopupService,
    ObjectTypeComponent,
    ObjectTypeDetailComponent,
    ObjectTypeDialogComponent,
    ObjectTypePopupComponent,
    ObjectTypeDeletePopupComponent,
    ObjectTypeDeleteDialogComponent,
    objectTypeRoute,
    objectTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...objectTypeRoute,
    ...objectTypePopupRoute,
];

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ObjectTypeComponent,
        ObjectTypeDetailComponent,
        ObjectTypeDialogComponent,
        ObjectTypeDeleteDialogComponent,
        ObjectTypePopupComponent,
        ObjectTypeDeletePopupComponent,
    ],
    entryComponents: [
        ObjectTypeComponent,
        ObjectTypeDialogComponent,
        ObjectTypePopupComponent,
        ObjectTypeDeleteDialogComponent,
        ObjectTypeDeletePopupComponent,
    ],
    providers: [
        ObjectTypeService,
        ObjectTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorObjectTypeModule {}
