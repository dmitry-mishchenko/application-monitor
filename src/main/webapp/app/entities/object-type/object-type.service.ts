import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { ObjectType } from './object-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ObjectType>;

@Injectable()
export class ObjectTypeService {

    private resourceUrl =  SERVER_API_URL + 'api/object-types';

    constructor(private http: HttpClient) { }

    create(objectType: ObjectType): Observable<EntityResponseType> {
        const copy = this.convert(objectType);
        return this.http.post<ObjectType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(objectType: ObjectType): Observable<EntityResponseType> {
        const copy = this.convert(objectType);
        return this.http.put<ObjectType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<ObjectType>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ObjectType[]>> {
        const options = createRequestOption(req);
        return this.http.get<ObjectType[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ObjectType[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ObjectType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ObjectType[]>): HttpResponse<ObjectType[]> {
        const jsonResponse: ObjectType[] = res.body;
        const body: ObjectType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ObjectType.
     */
    private convertItemFromServer(objectType: ObjectType): ObjectType {
        const copy: ObjectType = Object.assign({}, objectType);
        return copy;
    }

    /**
     * Convert a ObjectType to a JSON which can be sent to the server.
     */
    private convert(objectType: ObjectType): ObjectType {
        const copy: ObjectType = Object.assign({}, objectType);
        return copy;
    }
}
