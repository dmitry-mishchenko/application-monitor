import { BaseEntity } from './../../shared';

export class ObjectType implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
    ) {
    }
}
