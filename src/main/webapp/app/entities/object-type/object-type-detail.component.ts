import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ObjectType } from './object-type.model';
import { ObjectTypeService } from './object-type.service';

@Component({
    selector: 'jhi-object-type-detail',
    templateUrl: './object-type-detail.component.html'
})
export class ObjectTypeDetailComponent implements OnInit, OnDestroy {

    objectType: ObjectType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private objectTypeService: ObjectTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInObjectTypes();
    }

    load(id) {
        this.objectTypeService.find(id)
            .subscribe((objectTypeResponse: HttpResponse<ObjectType>) => {
                this.objectType = objectTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInObjectTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'objectTypeListModification',
            (response) => this.load(this.objectType.id)
        );
    }
}
