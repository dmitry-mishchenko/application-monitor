import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ObjectType } from './object-type.model';
import { ObjectTypePopupService } from './object-type-popup.service';
import { ObjectTypeService } from './object-type.service';

@Component({
    selector: 'jhi-object-type-dialog',
    templateUrl: './object-type-dialog.component.html'
})
export class ObjectTypeDialogComponent implements OnInit {

    objectType: ObjectType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private objectTypeService: ObjectTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.objectType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.objectTypeService.update(this.objectType));
        } else {
            this.subscribeToSaveResponse(
                this.objectTypeService.create(this.objectType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ObjectType>>) {
        result.subscribe((res: HttpResponse<ObjectType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ObjectType) {
        this.eventManager.broadcast({ name: 'objectTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-object-type-popup',
    template: ''
})
export class ObjectTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private objectTypePopupService: ObjectTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.objectTypePopupService
                    .open(ObjectTypeDialogComponent as Component, params['id']);
            } else {
                this.objectTypePopupService
                    .open(ObjectTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
