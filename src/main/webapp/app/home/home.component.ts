import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Subsystem, SubsystemService } from '../entities/subsystem';
import { Account, LoginModalService, Principal } from '../shared';
import { AddSubsystemDialogComponent } from './add-subsystem-dialog/add-subsystem-dialog.component';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.css'
    ]
})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    subsystems: Subsystem[];

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private modalService: NgbModal,
        private subsystemService: SubsystemService
    ) {
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
        this.loadSubsystems();
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
                this.loadSubsystems();
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    loadSubsystems() {
        this.subsystemService.query().subscribe(
            (res: HttpResponse<Subsystem[]>) => {
                this.subsystems = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    addSubsystem() {
        const modalRef = this.modalService.open(AddSubsystemDialogComponent);
        modalRef.componentInstance.onError = this.onError;
    }
}
