import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppmonitorSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';
import { SubsystemCardComponent } from './subsystem-card/subsystem-card.component';
import { AddSubsystemDialogComponent } from './add-subsystem-dialog/add-subsystem-dialog.component';

@NgModule({
    imports: [
        AppmonitorSharedModule,
        RouterModule.forChild([HOME_ROUTE])
    ],
    declarations: [
        HomeComponent,
        SubsystemCardComponent,
        AddSubsystemDialogComponent
    ],
    entryComponents: [
        SubsystemCardComponent,
        AddSubsystemDialogComponent
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppmonitorHomeModule { }
