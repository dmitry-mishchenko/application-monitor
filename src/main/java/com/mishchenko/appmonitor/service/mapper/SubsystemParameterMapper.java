package com.mishchenko.appmonitor.service.mapper;

import com.mishchenko.appmonitor.domain.*;
import com.mishchenko.appmonitor.service.dto.SubsystemParameterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SubsystemParameter and its DTO SubsystemParameterDTO.
 */
@Mapper(componentModel = "spring", uses = {SubsystemMapper.class})
public interface SubsystemParameterMapper extends EntityMapper<SubsystemParameterDTO, SubsystemParameter> {

    @Mapping(source = "subsystem.id", target = "subsystemId")
    SubsystemParameterDTO toDto(SubsystemParameter subsystemParameter);

    @Mapping(source = "subsystemId", target = "subsystem")
    SubsystemParameter toEntity(SubsystemParameterDTO subsystemParameterDTO);

    default SubsystemParameter fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubsystemParameter subsystemParameter = new SubsystemParameter();
        subsystemParameter.setId(id);
        return subsystemParameter;
    }
}
