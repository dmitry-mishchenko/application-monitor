package com.mishchenko.appmonitor.service.mapper;

import com.mishchenko.appmonitor.domain.*;
import com.mishchenko.appmonitor.service.dto.ObjectTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ObjectType and its DTO ObjectTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ObjectTypeMapper extends EntityMapper<ObjectTypeDTO, ObjectType> {



    default ObjectType fromId(Long id) {
        if (id == null) {
            return null;
        }
        ObjectType objectType = new ObjectType();
        objectType.setId(id);
        return objectType;
    }
}
