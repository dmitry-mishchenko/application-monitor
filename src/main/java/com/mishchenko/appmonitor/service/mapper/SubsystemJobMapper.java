package com.mishchenko.appmonitor.service.mapper;

import com.mishchenko.appmonitor.domain.*;
import com.mishchenko.appmonitor.service.dto.SubsystemJobDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity SubsystemJob and its DTO SubsystemJobDTO.
 */
@Mapper(componentModel = "spring", uses = {SubsystemMapper.class})
public interface SubsystemJobMapper extends EntityMapper<SubsystemJobDTO, SubsystemJob> {

    @Mapping(source = "subsystem.id", target = "subsystemId")
    SubsystemJobDTO toDto(SubsystemJob subsystemJob);

    @Mapping(source = "subsystemId", target = "subsystem")
    SubsystemJob toEntity(SubsystemJobDTO subsystemJobDTO);

    default SubsystemJob fromId(Long id) {
        if (id == null) {
            return null;
        }
        SubsystemJob subsystemJob = new SubsystemJob();
        subsystemJob.setId(id);
        return subsystemJob;
    }
}
