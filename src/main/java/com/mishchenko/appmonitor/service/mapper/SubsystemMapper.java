package com.mishchenko.appmonitor.service.mapper;

import com.mishchenko.appmonitor.domain.*;
import com.mishchenko.appmonitor.service.dto.SubsystemDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Subsystem and its DTO SubsystemDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubsystemMapper extends EntityMapper<SubsystemDTO, Subsystem> {

    default Subsystem fromId(Long id) {
        if (id == null) {
            return null;
        }
        Subsystem subsystem = new Subsystem();
        subsystem.setId(id);
        return subsystem;
    }
}
