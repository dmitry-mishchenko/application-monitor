package com.mishchenko.appmonitor.service.mapper;

import com.mishchenko.appmonitor.domain.*;
import com.mishchenko.appmonitor.service.dto.IssueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Issue and its DTO IssueDTO.
 */
@Mapper(componentModel = "spring", uses = { ObjectStatusMapper.class, SubsystemObjectMapper.class, UserMapper.class })
public interface IssueMapper extends EntityMapper<IssueDTO, Issue> {

    @Mapping(source = "objectStatus.id", target = "objectStatusId")
    @Mapping(source = "subsystemObject.id", target = "subsystemObjectId")
    @Mapping(source = "subsystemObject.name", target = "subsystemObjectName")
    @Mapping(source = "subsystemObject.description", target = "subsystemObjectDescription")
    @Mapping(source = "subsystemObject.objectType.description", target = "objectTypeDescription")
    @Mapping(source = "objectStatus.name", target = "objectStatusName")
    @Mapping(source = "objectStatus.description", target = "objectStatusDescription")
    @Mapping(source = "assignedUser.id", target = "assignedUserId")
    @Mapping(source = "assignedUser.login", target = "assignedUserLogin")
    @Mapping(source = "assignedUser.firstName", target = "assignedUserFirstName")
    @Mapping(source = "assignedUser.lastName", target = "assignedUserLastName")
    @Mapping(source = "assignedUser.email", target = "assignedUserEmail")
    IssueDTO toDto(Issue issue);

    @Mapping(source = "objectStatusId", target = "objectStatus")
    @Mapping(source = "subsystemObjectId", target = "subsystemObject")
    @Mapping(source = "assignedUserId", target = "assignedUser")
    Issue toEntity(IssueDTO issueDTO);

    default Issue fromId(Long id) {
        if (id == null) {
            return null;
        }
        Issue issue = new Issue();
        issue.setId(id);
        return issue;
    }
}
