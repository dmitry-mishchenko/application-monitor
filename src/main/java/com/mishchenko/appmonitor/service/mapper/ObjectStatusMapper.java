package com.mishchenko.appmonitor.service.mapper;

import com.mishchenko.appmonitor.domain.*;
import com.mishchenko.appmonitor.service.dto.ObjectStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ObjectStatus and its DTO ObjectStatusDTO.
 */
@Mapper(componentModel = "spring", uses = {ObjectTypeMapper.class})
public interface ObjectStatusMapper extends EntityMapper<ObjectStatusDTO, ObjectStatus> {

    @Mapping(source = "objectType.id", target = "objectTypeId")
    ObjectStatusDTO toDto(ObjectStatus objectStatus);

    @Mapping(source = "objectTypeId", target = "objectType")
    ObjectStatus toEntity(ObjectStatusDTO objectStatusDTO);

    default ObjectStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        ObjectStatus objectStatus = new ObjectStatus();
        objectStatus.setId(id);
        return objectStatus;
    }
}
