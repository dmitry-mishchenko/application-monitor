package com.mishchenko.appmonitor.service;

import com.mishchenko.appmonitor.service.dto.SubsystemParameterDTO;
import java.util.List;

/**
 * Service Interface for managing SubsystemParameter.
 */
public interface SubsystemParameterService {

    /**
     * Save a subsystemParameter.
     *
     * @param subsystemParameterDTO the entity to save
     * @return the persisted entity
     */
    SubsystemParameterDTO save(SubsystemParameterDTO subsystemParameterDTO);

    /**
     * Get all the subsystemParameters.
     *
     * @return the list of entities
     */
    List<SubsystemParameterDTO> findAll();

    /**
     * Get the "id" subsystemParameter.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SubsystemParameterDTO findOne(Long id);

    /**
     * Delete the "id" subsystemParameter.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
