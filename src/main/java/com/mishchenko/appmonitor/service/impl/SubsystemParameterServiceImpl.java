package com.mishchenko.appmonitor.service.impl;

import com.mishchenko.appmonitor.service.SubsystemParameterService;
import com.mishchenko.appmonitor.domain.SubsystemParameter;
import com.mishchenko.appmonitor.repository.SubsystemParameterRepository;
import com.mishchenko.appmonitor.service.dto.SubsystemParameterDTO;
import com.mishchenko.appmonitor.service.mapper.SubsystemParameterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing SubsystemParameter.
 */
@Service
@Transactional
public class SubsystemParameterServiceImpl implements SubsystemParameterService {

    private final Logger log = LoggerFactory.getLogger(SubsystemParameterServiceImpl.class);

    private final SubsystemParameterRepository subsystemParameterRepository;

    private final SubsystemParameterMapper subsystemParameterMapper;

    public SubsystemParameterServiceImpl(SubsystemParameterRepository subsystemParameterRepository, SubsystemParameterMapper subsystemParameterMapper) {
        this.subsystemParameterRepository = subsystemParameterRepository;
        this.subsystemParameterMapper = subsystemParameterMapper;
    }

    /**
     * Save a subsystemParameter.
     *
     * @param subsystemParameterDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SubsystemParameterDTO save(SubsystemParameterDTO subsystemParameterDTO) {
        log.debug("Request to save SubsystemParameter : {}", subsystemParameterDTO);
        SubsystemParameter subsystemParameter = subsystemParameterMapper.toEntity(subsystemParameterDTO);
        subsystemParameter = subsystemParameterRepository.save(subsystemParameter);
        return subsystemParameterMapper.toDto(subsystemParameter);
    }

    /**
     * Get all the subsystemParameters.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<SubsystemParameterDTO> findAll() {
        log.debug("Request to get all SubsystemParameters");
        return subsystemParameterRepository.findAll().stream()
            .map(subsystemParameterMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one subsystemParameter by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SubsystemParameterDTO findOne(Long id) {
        log.debug("Request to get SubsystemParameter : {}", id);
        SubsystemParameter subsystemParameter = subsystemParameterRepository.findOne(id);
        return subsystemParameterMapper.toDto(subsystemParameter);
    }

    /**
     * Delete the subsystemParameter by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubsystemParameter : {}", id);
        subsystemParameterRepository.delete(id);
    }
}
