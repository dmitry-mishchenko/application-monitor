package com.mishchenko.appmonitor.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import com.mishchenko.appmonitor.domain.SubsystemJob;
import com.mishchenko.appmonitor.repository.SubsystemJobRepository;
import com.mishchenko.appmonitor.service.SubsystemJobService;
import com.mishchenko.appmonitor.service.dto.SubsystemJobDTO;
import com.mishchenko.appmonitor.service.mapper.SubsystemJobMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing SubsystemJob.
 */
@Service
@Transactional
public class SubsystemJobServiceImpl implements SubsystemJobService {

    private final Logger log = LoggerFactory.getLogger(SubsystemJobServiceImpl.class);

    private final SubsystemJobRepository subsystemJobRepository;

    private final SubsystemJobMapper subsystemJobMapper;

    public SubsystemJobServiceImpl(SubsystemJobRepository subsystemJobRepository,
            SubsystemJobMapper subsystemJobMapper) {
        this.subsystemJobRepository = subsystemJobRepository;
        this.subsystemJobMapper = subsystemJobMapper;
    }

    /**
     * Save a subsystemJob.
     *
     * @param subsystemJobDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SubsystemJobDTO save(SubsystemJobDTO subsystemJobDTO) {
        log.debug("Request to save SubsystemJob : {}", subsystemJobDTO);
        SubsystemJob subsystemJob = subsystemJobMapper.toEntity(subsystemJobDTO);
        subsystemJob = subsystemJobRepository.save(subsystemJob);
        return subsystemJobMapper.toDto(subsystemJob);
    }

    /**
     * Get all the subsystemJobs.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubsystemJobDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubsystemJobs");
        return subsystemJobRepository.findAll(pageable).map(subsystemJobMapper::toDto);
    }

    /**
     * Get one subsystemJob by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SubsystemJobDTO findOne(Long id) {
        log.debug("Request to get SubsystemJob : {}", id);
        SubsystemJob subsystemJob = subsystemJobRepository.findOne(id);
        return subsystemJobMapper.toDto(subsystemJob);
    }

    /**
     * Get all subsystemJobs for one subsystem.
     *
     * @param id the id of the subsystem
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public List<SubsystemJobDTO> findAllBySubsystemId(Long id) {
        log.debug("Request to get all SUbsystemJobs for Subsystem : {}", id);
        return subsystemJobRepository.findAllBySubsystemId(id).stream().map(subsystemJobMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Delete the subsystemJob by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubsystemJob : {}", id);
        subsystemJobRepository.delete(id);
    }
}
