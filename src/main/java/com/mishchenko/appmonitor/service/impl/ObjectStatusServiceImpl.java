package com.mishchenko.appmonitor.service.impl;

import com.mishchenko.appmonitor.service.ObjectStatusService;
import com.mishchenko.appmonitor.domain.ObjectStatus;
import com.mishchenko.appmonitor.repository.ObjectStatusRepository;
import com.mishchenko.appmonitor.service.dto.ObjectStatusDTO;
import com.mishchenko.appmonitor.service.mapper.ObjectStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ObjectStatus.
 */
@Service
@Transactional
public class ObjectStatusServiceImpl implements ObjectStatusService {

    private final Logger log = LoggerFactory.getLogger(ObjectStatusServiceImpl.class);

    private final ObjectStatusRepository objectStatusRepository;

    private final ObjectStatusMapper objectStatusMapper;

    public ObjectStatusServiceImpl(ObjectStatusRepository objectStatusRepository, ObjectStatusMapper objectStatusMapper) {
        this.objectStatusRepository = objectStatusRepository;
        this.objectStatusMapper = objectStatusMapper;
    }

    /**
     * Save a objectStatus.
     *
     * @param objectStatusDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ObjectStatusDTO save(ObjectStatusDTO objectStatusDTO) {
        log.debug("Request to save ObjectStatus : {}", objectStatusDTO);
        ObjectStatus objectStatus = objectStatusMapper.toEntity(objectStatusDTO);
        objectStatus = objectStatusRepository.save(objectStatus);
        return objectStatusMapper.toDto(objectStatus);
    }

    /**
     * Get all the objectStatuses.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<ObjectStatusDTO> findAll() {
        log.debug("Request to get all ObjectStatuses");
        return objectStatusRepository.findAll().stream()
            .map(objectStatusMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one objectStatus by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public ObjectStatusDTO findOne(Long id) {
        log.debug("Request to get ObjectStatus : {}", id);
        ObjectStatus objectStatus = objectStatusRepository.findOne(id);
        return objectStatusMapper.toDto(objectStatus);
    }

    /**
     * Delete the objectStatus by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ObjectStatus : {}", id);
        objectStatusRepository.delete(id);
    }
}
