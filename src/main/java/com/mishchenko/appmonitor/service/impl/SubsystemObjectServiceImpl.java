package com.mishchenko.appmonitor.service.impl;

import com.mishchenko.appmonitor.service.SubsystemObjectService;
import com.mishchenko.appmonitor.domain.SubsystemObject;
import com.mishchenko.appmonitor.repository.SubsystemObjectRepository;
import com.mishchenko.appmonitor.service.dto.SubsystemObjectDTO;
import com.mishchenko.appmonitor.service.mapper.SubsystemObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing SubsystemObject.
 */
@Service
@Transactional
public class SubsystemObjectServiceImpl implements SubsystemObjectService {

    private final Logger log = LoggerFactory.getLogger(SubsystemObjectServiceImpl.class);

    private final SubsystemObjectRepository subsystemObjectRepository;

    private final SubsystemObjectMapper subsystemObjectMapper;

    public SubsystemObjectServiceImpl(SubsystemObjectRepository subsystemObjectRepository, SubsystemObjectMapper subsystemObjectMapper) {
        this.subsystemObjectRepository = subsystemObjectRepository;
        this.subsystemObjectMapper = subsystemObjectMapper;
    }

    /**
     * Save a subsystemObject.
     *
     * @param subsystemObjectDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public SubsystemObjectDTO save(SubsystemObjectDTO subsystemObjectDTO) {
        log.debug("Request to save SubsystemObject : {}", subsystemObjectDTO);
        SubsystemObject subsystemObject = subsystemObjectMapper.toEntity(subsystemObjectDTO);
        subsystemObject = subsystemObjectRepository.save(subsystemObject);
        return subsystemObjectMapper.toDto(subsystemObject);
    }

    /**
     * Get all the subsystemObjects.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<SubsystemObjectDTO> findAll(Pageable pageable) {
        log.debug("Request to get all SubsystemObjects");
        return subsystemObjectRepository.findAll(pageable)
            .map(subsystemObjectMapper::toDto);
    }

    /**
     * Get one subsystemObject by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public SubsystemObjectDTO findOne(Long id) {
        log.debug("Request to get SubsystemObject : {}", id);
        SubsystemObject subsystemObject = subsystemObjectRepository.findOne(id);
        return subsystemObjectMapper.toDto(subsystemObject);
    }

    /**
     * Delete the subsystemObject by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete SubsystemObject : {}", id);
        subsystemObjectRepository.delete(id);
    }
}
