package com.mishchenko.appmonitor.service;

import com.mishchenko.appmonitor.service.dto.ObjectStatusDTO;
import java.util.List;

/**
 * Service Interface for managing ObjectStatus.
 */
public interface ObjectStatusService {

    /**
     * Save a objectStatus.
     *
     * @param objectStatusDTO the entity to save
     * @return the persisted entity
     */
    ObjectStatusDTO save(ObjectStatusDTO objectStatusDTO);

    /**
     * Get all the objectStatuses.
     *
     * @return the list of entities
     */
    List<ObjectStatusDTO> findAll();

    /**
     * Get the "id" objectStatus.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ObjectStatusDTO findOne(Long id);

    /**
     * Delete the "id" objectStatus.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
