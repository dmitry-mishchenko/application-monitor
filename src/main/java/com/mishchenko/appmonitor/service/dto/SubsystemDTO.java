package com.mishchenko.appmonitor.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

import javax.persistence.Lob;

import com.mishchenko.appmonitor.domain.SubsystemInformation;
import com.mishchenko.appmonitor.domain.enumeration.SubsystemType;
import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;

/**
 * A DTO for the Subsystem entity.
 */
public class SubsystemDTO implements Serializable {

    private Long id;

    private String name;

    private SubsystemType subsystemType;

    private Integer refreshRate;

    private ZonedDateTime updatedAt;

    @Lob
    private SubsystemInformation information;

    private ZConnectionParameters connectionParameters;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubsystemType getSubsystemType() {
        return subsystemType;
    }

    public void setSubsystemType(SubsystemType subsystemType) {
        this.subsystemType = subsystemType;
    }

    public Integer getRefreshRate() {
        return refreshRate;
    }

    public void setRefreshRate(Integer refreshRate) {
        this.refreshRate = refreshRate;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public SubsystemInformation getInformation() {
        return information;
    }

    public void setInformation(SubsystemInformation information) {
        this.information = information;
    }

    public ZConnectionParameters getConnectionParameters() {
        return connectionParameters;
    }

    public void setConnectionParameters(ZConnectionParameters connectionParameters) {
        this.connectionParameters = connectionParameters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubsystemDTO subsystemDTO = (SubsystemDTO) o;
        if (subsystemDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subsystemDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubsystemDTO{" + "id=" + getId() + ", name='" + getName() + "'" + ", subsystemType='"
                + getSubsystemType() + "'" + ", refreshRate=" + getRefreshRate() + ", updatedAt='" + getUpdatedAt()
                + "'" + ", information='" + getInformation() + "'" + "}";
    }
}
