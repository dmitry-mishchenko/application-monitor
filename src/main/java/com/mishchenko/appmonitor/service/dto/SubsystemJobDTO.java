package com.mishchenko.appmonitor.service.dto;


import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import javax.persistence.Lob;
import com.mishchenko.appmonitor.domain.enumeration.JesJobStatus;
import com.mishchenko.appmonitor.domain.enumeration.SubsystemJobType;

/**
 * A DTO for the SubsystemJob entity.
 */
public class SubsystemJobDTO implements Serializable {

    private Long id;

    private String name;

    private String description;

    private JesJobStatus status;

    private SubsystemJobType type;

    private ZonedDateTime startedAt;

    @Lob
    private String log;

    private Long subsystemId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JesJobStatus getStatus() {
        return status;
    }

    public void setStatus(JesJobStatus status) {
        this.status = status;
    }

    public SubsystemJobType getType() {
        return type;
    }

    public void setType(SubsystemJobType type) {
        this.type = type;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public Long getSubsystemId() {
        return subsystemId;
    }

    public void setSubsystemId(Long subsystemId) {
        this.subsystemId = subsystemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubsystemJobDTO subsystemJobDTO = (SubsystemJobDTO) o;
        if(subsystemJobDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subsystemJobDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubsystemJobDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", status='" + getStatus() + "'" +
            ", type='" + getType() + "'" +
            ", startedAt='" + getStartedAt() + "'" +
            ", log='" + getLog() + "'" +
            "}";
    }
}
