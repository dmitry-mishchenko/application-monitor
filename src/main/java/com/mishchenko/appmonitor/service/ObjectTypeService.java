package com.mishchenko.appmonitor.service;

import com.mishchenko.appmonitor.service.dto.ObjectTypeDTO;
import java.util.List;

/**
 * Service Interface for managing ObjectType.
 */
public interface ObjectTypeService {

    /**
     * Save a objectType.
     *
     * @param objectTypeDTO the entity to save
     * @return the persisted entity
     */
    ObjectTypeDTO save(ObjectTypeDTO objectTypeDTO);

    /**
     * Get all the objectTypes.
     *
     * @return the list of entities
     */
    List<ObjectTypeDTO> findAll();

    /**
     * Get the "id" objectType.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ObjectTypeDTO findOne(Long id);

    /**
     * Delete the "id" objectType.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
