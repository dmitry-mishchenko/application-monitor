package com.mishchenko.appmonitor.service;

import com.mishchenko.appmonitor.service.dto.SubsystemObjectDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing SubsystemObject.
 */
public interface SubsystemObjectService {

    /**
     * Save a subsystemObject.
     *
     * @param subsystemObjectDTO the entity to save
     * @return the persisted entity
     */
    SubsystemObjectDTO save(SubsystemObjectDTO subsystemObjectDTO);

    /**
     * Get all the subsystemObjects.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<SubsystemObjectDTO> findAll(Pageable pageable);

    /**
     * Get the "id" subsystemObject.
     *
     * @param id the id of the entity
     * @return the entity
     */
    SubsystemObjectDTO findOne(Long id);

    /**
     * Delete the "id" subsystemObject.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
