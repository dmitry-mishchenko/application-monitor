package com.mishchenko.appmonitor.domain.enumeration;

/**
 * The JesJobLogType enumeration.
 */
public enum JesJobLogType {
    JESMSGLG, JESJCL, JESYSMSG, OTHER
}
