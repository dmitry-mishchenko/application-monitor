package com.mishchenko.appmonitor.domain.enumeration;

/**
 * The JesJobStatus enumeration.
 */
public enum JesJobStatus {
    ACTIVE, STOPPED
}
