package com.mishchenko.appmonitor.domain;

import java.io.Serializable;
import java.util.List;

import com.mishchenko.appmonitor.zos.util.ims.ImsExternalSubsystem;
import com.mishchenko.appmonitor.zos.util.ims.ImsQueuedMessage;

public class SubsystemInformation implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<ImsExternalSubsystem> imsExternalSubsystems;

	private List<ImsQueuedMessage> imsQueuedMessages;

	public void setImsExternalSubsystems(List<ImsExternalSubsystem> imsExternalSubsystems) {
		this.imsExternalSubsystems = imsExternalSubsystems;
	}

	public List<ImsExternalSubsystem> getImsExternalSubsystems() {
		return imsExternalSubsystems;
	}

	public void setImsQueuedMessages(List<ImsQueuedMessage> imsQueuedMessages) {
		this.imsQueuedMessages = imsQueuedMessages;
	}

	public List<ImsQueuedMessage> getImsQueuedMessages() {
		return imsQueuedMessages;
	}

	@Override
	public String toString() {
		return "SubsystemInformation{" + "imsExternalSubsystems='" + getImsExternalSubsystems()
				+ "', imsQueuedMessages='" + getImsQueuedMessages() + "'}";
	}
}