package com.mishchenko.appmonitor.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Issue.
 */
@Entity
@Table(name = "issue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Issue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "occured_at")
    private ZonedDateTime occuredAt;

    @Column(name = "resolved_at")
    private ZonedDateTime resolvedAt;

    @ManyToOne
    private ObjectStatus objectStatus;

    @ManyToOne
    private SubsystemObject subsystemObject;

    @ManyToOne
    private User assignedUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getOccuredAt() {
        return occuredAt;
    }

    public Issue occuredAt(ZonedDateTime occuredAt) {
        this.occuredAt = occuredAt;
        return this;
    }

    public void setOccuredAt(ZonedDateTime occuredAt) {
        this.occuredAt = occuredAt;
    }

    public ZonedDateTime getResolvedAt() {
        return resolvedAt;
    }

    public Issue resolvedAt(ZonedDateTime resolvedAt) {
        this.resolvedAt = resolvedAt;
        return this;
    }

    public void setResolvedAt(ZonedDateTime resolvedAt) {
        this.resolvedAt = resolvedAt;
    }

    public ObjectStatus getObjectStatus() {
        return objectStatus;
    }

    public Issue objectStatus(ObjectStatus objectStatus) {
        this.objectStatus = objectStatus;
        return this;
    }

    public void setObjectStatus(ObjectStatus objectStatus) {
        this.objectStatus = objectStatus;
    }

    public SubsystemObject getSubsystemObject() {
        return subsystemObject;
    }

    public Issue subsystemObject(SubsystemObject subsystemObject) {
        this.subsystemObject = subsystemObject;
        return this;
    }

    public void setSubsystemObject(SubsystemObject subsystemObject) {
        this.subsystemObject = subsystemObject;
    }

    public User getAssignedUser() {
        return assignedUser;
    }

    public Issue assignedUser(User user) {
        this.assignedUser = user;
        return this;
    }

    public void setAssignedUser(User user) {
        this.assignedUser = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Issue issue = (Issue) o;
        if (issue.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), issue.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Issue{" +
            "id=" + getId() +
            ", occuredAt='" + getOccuredAt() + "'" +
            ", resolvedAt='" + getResolvedAt() + "'" +
            "}";
    }
}
