package com.mishchenko.appmonitor.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SubsystemParameter.
 */
@Entity
@Table(name = "subsystem_parameter")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SubsystemParameter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "parameter_key")
    private String parameterKey;

    @Column(name = "parameter_value")
    private String parameterValue;

    @ManyToOne
    private Subsystem subsystem;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParameterKey() {
        return parameterKey;
    }

    public SubsystemParameter parameterKey(String parameterKey) {
        this.parameterKey = parameterKey;
        return this;
    }

    public void setParameterKey(String parameterKey) {
        this.parameterKey = parameterKey;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public SubsystemParameter parameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
        return this;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public SubsystemParameter subsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
        return this;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SubsystemParameter subsystemParameter = (SubsystemParameter) o;
        if (subsystemParameter.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subsystemParameter.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubsystemParameter{" +
            "id=" + getId() +
            ", parameterKey='" + getParameterKey() + "'" +
            ", parameterValue='" + getParameterValue() + "'" +
            "}";
    }
}
