package com.mishchenko.appmonitor.zos.service.impl;

import java.util.Map;
import java.util.stream.Collectors;

import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.domain.SubsystemParameter;
import com.mishchenko.appmonitor.repository.SubsystemParameterRepository;
import com.mishchenko.appmonitor.zos.connection.ImsConnection;
import com.mishchenko.appmonitor.zos.connection.JesConnection;
import com.mishchenko.appmonitor.zos.service.ConnectionFactoryService;
import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service implementation for creating connections.
 */
@Service
@Transactional
public class ConnectionFactoryServiceImpl implements ConnectionFactoryService {

	private static final Logger log = LoggerFactory.getLogger(ConnectionFactoryServiceImpl.class);

	private final SubsystemParameterRepository subsystemParameterRepository;

	public ConnectionFactoryServiceImpl(SubsystemParameterRepository subsystemParameterRepository) {
		this.subsystemParameterRepository = subsystemParameterRepository;
	}

	@Override
	public ImsConnection getImsConnection(Subsystem subsystem) throws Exception {
		return new ImsConnection(this.getParametersBySubsystem(subsystem));
	}

	@Override
	public JesConnection getJesConnection(Subsystem subsystem) throws Exception {
		return new JesConnection(this.getParametersBySubsystem(subsystem));
	}

	@Override
	public ZConnectionParameters getParametersBySubsystem(Subsystem subsystem) {
		ZConnectionParameters connectionParameters = new ZConnectionParameters();
		Map<String, String> parametersMap = this.getParametersMapBySubsystem(subsystem);
		connectionParameters.setSubsystem(subsystem);
		connectionParameters.setHostname(parametersMap.get("HOSTNAME"));
		connectionParameters.setPort(parametersMap.get("PORT"));
		connectionParameters.setUserId(parametersMap.get("USERID"));
		connectionParameters.setPassword(parametersMap.get("PASSWORD"));
		connectionParameters.setImsDatastore(parametersMap.get("IMS_DATASTORE"));
		connectionParameters.setImsExitIdentifier(parametersMap.get("IMS_EXIT_IDENTIFIER"));
		connectionParameters.setJobAccountingInfo(parametersMap.get("JOB_ACCOUNTING_INFO"));
		connectionParameters.setJobClass(parametersMap.get("JOB_CLASS"));
		return connectionParameters;
	}

	private Map<String, String> getParametersMapBySubsystem(Subsystem subsystem) {
		return this.subsystemParameterRepository.findAllBySubsystemId(subsystem.getId()).stream()
				.collect(Collectors.toMap(SubsystemParameter::getParameterKey, SubsystemParameter::getParameterValue));
	}
}
