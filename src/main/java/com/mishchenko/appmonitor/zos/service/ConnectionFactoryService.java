package com.mishchenko.appmonitor.zos.service;

import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.zos.connection.ImsConnection;
import com.mishchenko.appmonitor.zos.connection.JesConnection;
import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;

import org.springframework.stereotype.Service;

/**
 * Service Interface for creating connections.
 */
@Service
public interface ConnectionFactoryService {

    ImsConnection getImsConnection(Subsystem subsystem) throws Exception;

    JesConnection getJesConnection(Subsystem subsystem) throws Exception;

    ZConnectionParameters getParametersBySubsystem(Subsystem subsystem);
}
