package com.mishchenko.appmonitor.zos.service;

import com.mishchenko.appmonitor.domain.Subsystem;

import org.springframework.stereotype.Service;

/**
 * Service Interface for executing commands on remote subsystem.
 */
@Service
public interface CommandExecuteService {

    String executeImsCommand(Subsystem subsystem, String command) throws Exception;
}
