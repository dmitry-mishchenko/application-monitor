package com.mishchenko.appmonitor.zos.service.impl;

import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.mishchenko.appmonitor.domain.Issue;
import com.mishchenko.appmonitor.domain.ObjectStatus;
import com.mishchenko.appmonitor.domain.ObjectType;
import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.domain.SubsystemObject;
import com.mishchenko.appmonitor.repository.IssueRepository;
import com.mishchenko.appmonitor.repository.ObjectStatusRepository;
import com.mishchenko.appmonitor.repository.SubsystemObjectRepository;
import com.mishchenko.appmonitor.zos.service.IssueUpdateService;
import com.mishchenko.appmonitor.zos.util.ZObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for processing issues.
 */
@Service
@Transactional
public class IssueUpdateServiceImpl implements IssueUpdateService {

	private static final Logger log = LoggerFactory.getLogger(IssueUpdateServiceImpl.class);

	private final SubsystemObjectRepository subsystemObjectRepository;

	private final ObjectStatusRepository objectStatusRepository;

	private final IssueRepository issueRepository;

	public IssueUpdateServiceImpl(IssueRepository issueRepository, ObjectStatusRepository objectStatusRepository,
			SubsystemObjectRepository subsystemObjectRepository) {
		this.issueRepository = issueRepository;
		this.objectStatusRepository = objectStatusRepository;
		this.subsystemObjectRepository = subsystemObjectRepository;
	}

	@Override
	public List<Issue> checkObjects(List<ZObject> zObjects, Subsystem subsystem) throws Exception {
		List<SubsystemObject> subsystemObjects = this.subsystemObjectRepository.findAllBySubsystemId(subsystem.getId());
		List<ObjectStatus> objectStatuses = this.objectStatusRepository.findAll();
		List<Issue> issues = this.issueRepository.findAll();

		List<SubsystemObject> updatedObjects = new LinkedList<>();
		List<Issue> updatedIssues = new LinkedList<>();

		zObjects.forEach(zObject -> {
			String name = zObject.getName();
			String type = zObject.getType();
			String status = zObject.getStatus();
			SubsystemObject subsystemObject = this.findSubsystemObjectInList(subsystemObjects, name, type);
			if (subsystemObject != null) {
				updatedObjects.add(subsystemObject);
				ObjectStatus objectStatus = this.findObjectStatusInList(objectStatuses, status, type);
				if (objectStatus != null) {
					Issue issue = this.findUnresolvedIssueInList(issues, subsystemObject);
					if (issue == null) {
						issue = this.createIssue(subsystemObject, objectStatus);
						updatedIssues.add(issue);
					} else if (!issue.getObjectStatus().equals(objectStatus)) {
						this.closeIssue(issue);
						issue = this.createIssue(subsystemObject, objectStatus);
						updatedIssues.add(issue);
					}
				}
			}
		});

		issues.stream().filter(issue -> issue.getResolvedAt() == null).forEach(issue -> {
			if (!updatedObjects.contains(issue.getSubsystemObject())) {
				issue.setResolvedAt(ZonedDateTime.now());
				issueRepository.saveAndFlush(issue);
			}
		});

		return updatedIssues;
	}

	private Issue createIssue(SubsystemObject subsystemObject, ObjectStatus objectStatus) {
		Issue issue = new Issue();
		issue.setSubsystemObject(subsystemObject);
		issue.setObjectStatus(objectStatus);
		issue.setOccuredAt(ZonedDateTime.now());
		issueRepository.saveAndFlush(issue);
		log.info("Created new Issue: {}", issue);
		return issue;
	}

	private Issue closeIssue(Issue issue) {
		issue.setResolvedAt(ZonedDateTime.now());
		issueRepository.saveAndFlush(issue);
		log.info("Closed Issue: {}", issue);
		return issue;
	}

	private ObjectStatus createObjectStatus(String name, String description, ObjectType objectType) {
		ObjectStatus objectStatus = new ObjectStatus();
		objectStatus.setName(name);
		objectStatus.setDescription(description);
		objectStatus.setObjectType(objectType);
		objectStatusRepository.save(objectStatus);
		log.info("Created new ObjectStatus: {}", objectStatus);
		return objectStatus;
	}

	private SubsystemObject findSubsystemObjectInList(List<SubsystemObject> subsystemObjects, String name,
			String type) {
		Optional<SubsystemObject> subsystemObjectOptional = subsystemObjects.stream()
				.filter(subsystemObject -> subsystemObject.getName().equalsIgnoreCase(name)
						&& subsystemObject.getObjectType().getName().equalsIgnoreCase(type))
				.findFirst();
		return subsystemObjectOptional.isPresent() ? subsystemObjectOptional.get() : null;
	}

	private ObjectStatus findObjectStatusInList(List<ObjectStatus> objectStatuses, String status, String objectType) {
		Optional<ObjectStatus> objectStatusOptional = objectStatuses.stream()
				.filter(objectStatus -> objectStatus.getName().equalsIgnoreCase(status)
						&& objectStatus.getObjectType().getName().equalsIgnoreCase(objectType))
				.findFirst();
		if (objectStatusOptional.isPresent()) {
			return objectStatusOptional.get();
		} else {
			ObjectStatus objectStatus = this.statusStringContainsObjectStatusNameFromList(status, objectStatuses,
					objectType);
			if (objectStatus != null) {
				this.createObjectStatus(status, objectStatus.getDescription(), objectStatus.getObjectType());
				return objectStatus;
			} else {
				return null;
			}
		}
	}

	private Issue findUnresolvedIssueInList(List<Issue> issues, SubsystemObject subsystemObject) {
		Optional<Issue> issueOptional = issues.stream()
				.filter(issue -> issue.getSubsystemObject().equals(subsystemObject) && issue.getResolvedAt() == null)
				.findFirst();
		return issueOptional.isPresent() ? issueOptional.get() : null;
	}

	private ObjectStatus statusStringContainsObjectStatusNameFromList(String status, List<ObjectStatus> objectStatuses,
			String objectType) {
		Optional<ObjectStatus> objectStatusOptional = objectStatuses.parallelStream()
				.filter(objectStatus -> status.contains(objectStatus.getName())
						&& objectStatus.getObjectType().getName().equalsIgnoreCase(objectType))
				.findAny();
		return objectStatusOptional.isPresent() ? objectStatusOptional.get() : null;
	}
}
