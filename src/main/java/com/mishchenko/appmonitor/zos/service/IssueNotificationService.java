package com.mishchenko.appmonitor.zos.service;

import java.util.Collection;

import com.mishchenko.appmonitor.domain.Issue;

public interface IssueNotificationService {

    void notify(Issue issue) throws Exception;

    void notify(Collection<Issue> issues) throws Exception;
}
