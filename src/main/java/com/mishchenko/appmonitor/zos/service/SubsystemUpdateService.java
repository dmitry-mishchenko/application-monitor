package com.mishchenko.appmonitor.zos.service;

import com.mishchenko.appmonitor.domain.Subsystem;

import org.springframework.stereotype.Service;

/**
 * Service Interface for updating subsystem objects and information.
 */
@Service
public interface SubsystemUpdateService {

    /**
     * Update subsystem issues.
     */
    void updateIssues(Subsystem subsystem) throws Exception;

    /**
     * Update subsystem inforamtion.
     */
    void updateInformation(Subsystem subsystem) throws Exception;

    /**
     * Update subsystem job statuses.
     */
    void updateJobStatuses(Subsystem subsystem) throws Exception;
}
