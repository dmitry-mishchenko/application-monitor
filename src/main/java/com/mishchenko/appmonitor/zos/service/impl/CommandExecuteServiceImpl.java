package com.mishchenko.appmonitor.zos.service.impl;

import com.mishchenko.appmonitor.domain.Subsystem;
import com.mishchenko.appmonitor.zos.connection.ImsConnection;
import com.mishchenko.appmonitor.zos.service.CommandExecuteService;
import com.mishchenko.appmonitor.zos.service.ConnectionFactoryService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service implementation for executing commands on remote subsystem.
 */
@Service
@Transactional
public class CommandExecuteServiceImpl implements CommandExecuteService {

    private static final Logger log = LoggerFactory.getLogger(CommandExecuteServiceImpl.class);

    private final ConnectionFactoryService connectionFactoryService;

    public CommandExecuteServiceImpl(ConnectionFactoryService connectionFactoryService) {
        this.connectionFactoryService = connectionFactoryService;
    }

    @Override
    public String executeImsCommand(Subsystem subsystem, String command) throws Exception {
        ImsConnection imsConnection = connectionFactoryService.getImsConnection(subsystem);
        return imsConnection.execute(command);
    }
}
