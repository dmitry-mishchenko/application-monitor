package com.mishchenko.appmonitor.zos.connection;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.mishchenko.appmonitor.domain.SubsystemJob;
import com.mishchenko.appmonitor.domain.enumeration.JesJobLogType;
import com.mishchenko.appmonitor.domain.enumeration.JesJobStatus;
import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;
import com.mishchenko.appmonitor.zos.util.ZJobUtil;

import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JesConnection extends FtpConnection {

	private static final Logger log = LoggerFactory.getLogger(JesConnection.class);

	/* Used for a connection to a JES subsystem */
	private static final String FTP_FILETYPE_JES = "filetype=jes";

	/* Pattern to get JobID value from a JES response */
	private static final Pattern JES_JOB_ID = Pattern.compile("250-It is known to JES as (.*)");

	/* Will ask server once in 1 seconds */
	private static final long WAIT_INTERVAL = 1000;

	private ZConnectionParameters connectionParameters;

	public JesConnection(ZConnectionParameters connectionParameters) {
		super(connectionParameters);
		this.connectionParameters = connectionParameters;
	}

	public String submit(InputStream inputStream, boolean wait, int waitTime, boolean deleteFromSpool)
			throws Exception {
		if (!ftpClient.isConnected()) {
			login();
		}
		ftpClient.site(FTP_FILETYPE_JES);
		ftpClient.storeFile(this.hostname, inputStream);
		String jobId = getJobIdFromJesResponse();
		if (wait)
			waitForCompletion(jobId, waitTime);
		if (deleteFromSpool)
			ftpClient.deleteFile(jobId);
		return jobId;
	}

	private String getJobIdFromJesResponse() throws Exception {
		for (String replyString : ftpClient.getReplyStrings()) {
			Matcher matcher = JES_JOB_ID.matcher(replyString);
			if (matcher.matches()) {
				String jobId = matcher.group(1);
				log.info("Job submitted: {}", jobId);
				return jobId;
			}
		}
		throw new Exception("Job not accepted by JES. FTP reply code: " + ftpClient.getReply());
	}

	private void waitForCompletion(String jobId, long waitTime) throws Exception {
		waitTime = ((long) waitTime) * 1000;
		long currentTime = System.currentTimeMillis();
		long waitEndTime = currentTime + ((long) waitTime) * 1000;
		boolean eternal = (waitTime == 0);
		do {
			Thread.sleep(WAIT_INTERVAL);
			currentTime = System.currentTimeMillis();
			if (checkJobCompletion(jobId))
				break;
		} while (eternal || (currentTime <= waitEndTime));
	}

	private boolean checkJobCompletion(String jobId) {
		try {
			for (FTPFile ftpFile : ftpClient.listFiles("*")) {
				if (ftpFile.getRawListing().contains(jobId)) {
					if (ftpFile.getRawListing().contains("OUTPUT")) {
						log.info("Job completed: {}", jobId);
						return true;
					} else {
						log.debug("Wait for job ending: {}", jobId);
						return false;
					}
				}
			}
			log.debug("Wait for job ending: {}", jobId);
			return false;
		} catch (IOException e) {
			return false;
		}
	}

	public List<SubsystemJob> getJobsInformation(List<SubsystemJob> subsystemJobs) throws Exception {
		List<String> jobNames = subsystemJobs.stream().map(subsystemJob -> subsystemJob.getName())
				.collect(Collectors.toList());
		List<String> activeJobNames = this.getJobStatuses(jobNames);
		for (SubsystemJob subsystemJob : subsystemJobs) {
			if (activeJobNames.contains(subsystemJob.getName())) {
				subsystemJob.setStatus(JesJobStatus.ACTIVE);
				subsystemJob.setLog(this.getJobLog(subsystemJob.getName()));
				subsystemJob.setStartedAt(getJobStartTime(subsystemJob.getName()));
				log.debug("Subsystem job {} is active", subsystemJob.getName());
			} else {
				subsystemJob.setStatus(JesJobStatus.STOPPED);
				subsystemJob.setLog(null);
				subsystemJob.setStartedAt(null);
				log.debug("Subsystem job {} is stopped", subsystemJob.getName());
			}
		}
		return subsystemJobs;
	}

	private List<String> getJobStatuses(List<String> jobNames) throws Exception {
		this.submit(ZJobUtil.prepareJesJobStatusesJcl(jobNames, this.connectionParameters), true, 0, true);
		return this.retrieveFile("MONITOR." + this.connectionParameters.getSubsystem().getName() + ".JOBSTAT");
	}

	private String getJobLog(String jobName) throws Exception {
		List<String> jobLog = new ArrayList<>();
		for (JesJobLogType logType : JesJobLogType.values()) {
			String remoteFileName = "MONITOR." + jobName + "." + logType;
			try {
				jobLog.addAll(retrieveFile(remoteFileName));
			} catch (Exception e) {
				log.info("No such file on remote: {}", remoteFileName);
			}
		}
		return String.join("\n", jobLog);
	}

	private ZonedDateTime getJobStartTime(String jobName) throws Exception {
		String remoteFileName = "MONITOR." + jobName + ".START";
		List<String> jobStartTime = this.retrieveFile(remoteFileName);
		DateFormat format = new SimpleDateFormat("dd MMM yyyy hh.mm.ss", Locale.ENGLISH);
		Date date = format.parse(jobStartTime.get(0));
		return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}
}
