package com.mishchenko.appmonitor.zos.connection;

import java.net.SocketException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ibm.ims.connect.ApiProperties;
import com.ibm.ims.connect.Connection;
import com.ibm.ims.connect.ConnectionFactory;
import com.ibm.ims.connect.ImsConnectApiException;
import com.ibm.ims.connect.InputMessage;
import com.ibm.ims.connect.OutputMessage;
import com.ibm.ims.connect.TmInteraction;
import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;
import com.mishchenko.appmonitor.zos.util.ZObject;
import com.mishchenko.appmonitor.zos.util.ims.ImsExternalSubsystem;
import com.mishchenko.appmonitor.zos.util.ims.ImsQueuedMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IMS Connect API connection.
 */
public class ImsConnection {

	/* Logger */
	private static final Logger log = LoggerFactory.getLogger(ImsConnection.class);

	/* IMS Connect API connection factory */
	private ConnectionFactory connectionFactory;

	/* IMS Connect API connection */
	private Connection connection;

	/* IMS Connect API interaction entity */
	private TmInteraction tmInteraction;

	/* IMS Connect API output message */
	private OutputMessage outputMessage;

	/* IMS hostname */
	private String hostname;

	/* IMS port */
	private int port;

	/* IMS datastore */
	private String imsDatastore;

	/* IMS user message exit identifier */
	private String imsExitIdentifier;

	/* RACF userID */
	private String userId;

	/* RACF password */
	private String password;

	public ImsConnection(ZConnectionParameters connectionParameters) throws Exception {
		try {
			this.hostname = connectionParameters.getHostname();
			this.port = Integer.parseInt(connectionParameters.getPort());
			this.userId = connectionParameters.getUserId();
			this.password = connectionParameters.getPassword();
			this.imsDatastore = connectionParameters.getImsDatastore();
			this.imsExitIdentifier = connectionParameters.getImsExitIdentifier();

			/* Creating connection and interaction */
			this.connectionFactory = new ConnectionFactory();
			this.connectionFactory.setHostName(hostname);
			this.connectionFactory.setPortNumber(port);
			this.connectionFactory.setSocketType(ConnectionFactory.SOCKET_TYPE_PERSISTENT);

			this.connection = this.connectionFactory.getConnection();
			this.tmInteraction = this.connection.createInteraction();

			/* Interaction settings */
			this.tmInteraction.setImsConnectTimeout(10000);
			this.tmInteraction.setInteractionTimeout(60000);
			this.tmInteraction.setTrancode("");
			this.tmInteraction.setInputMessageDataSegmentsIncludeLlzzAndTrancode(false);
			this.tmInteraction.setCommitMode(ApiProperties.COMMIT_MODE_0);
			this.tmInteraction.setResponseIncludesLlll(true);
			this.tmInteraction.setInteractionTypeDescription(ApiProperties.INTERACTION_TYPE_DESC_SENDRECV);
			this.tmInteraction.setImsConnectUserMessageExitIdentifier(imsExitIdentifier);
			this.tmInteraction.setImsDatastoreName(imsDatastore);

			/* RACF settings */
			this.tmInteraction.setRacfUserId(userId);
			this.tmInteraction.setRacfPassword(password);
			this.tmInteraction.setRacfGroupName("");
			this.tmInteraction.setRacfApplName("");
		} catch (Exception e) {
			throw new Exception("Can't initialize IMS Connection", e);
		}
	}

	public void connect() throws Exception {
		try {
			log.info("Connecting to: {}:{}", this.hostname, this.port);
			this.connection.connect();
		} catch (SocketException e) {
			throw new Exception("Can't initialize IMS Connection. Can't open socket", e);
		} catch (ImsConnectApiException e) {
			throw new Exception("Can't initialize IMS Connection. IMS Connect API message: " + e.getMessage(), e);
		}
	}

	public void disconnect() throws Exception {
		try {
			log.info("Disconnecting from: {}:{}", this.hostname, this.port);
			this.connection.disconnect();
		} catch (ImsConnectApiException e) {
			throw new Exception("Can't close IMS connection", e);
		}
	}

	public String execute(String command) throws Exception {
		try {
			log.info("{}:{} Executing command: '{}'", this.hostname, this.port, command);
			InputMessage inputMessage = this.tmInteraction.getInputMessage();
			inputMessage.setInputMessageData(command.getBytes(tmInteraction.getImsConnectCodepage()));
			this.tmInteraction.execute();
			outputMessage = tmInteraction.getOutputMessage();
			return outputMessage.getDataAsString();
		} catch (Exception e) {
			throw new Exception("Error while executing command. IMS Connect API message: " + e.getMessage(), e);
		}
	}

	public List<ZObject> getTransactionStatuses() throws Exception {
		this.execute("/DIS STATUS TRANSACTION");
		List<String> response = Arrays.asList(this.outputMessage.getDataAsArrayOfStrings());
		List<ZObject> transactions = new LinkedList<>();
		for (int i = 1; !response.get(i).trim().startsWith("*"); i++) {
			Pattern pattern = Pattern.compile("(\\w+)\\s+(\\w+)\\s+(\\w+.+)");
			Matcher matcher = pattern.matcher(response.get(i).trim());
			if (matcher.matches()) {
				ZObject transaction = new ZObject();
				transaction.setName(matcher.group(1));
				transaction.setType("IMS_TRANSACTION");
				transaction.setStatus(matcher.group(3));
				transactions.add(transaction);
			}
		}
		return transactions;
	}

	public List<ZObject> getDatabaseStatuses() throws Exception {
		this.execute("/DIS STATUS DATABASE");
		List<String> response = Arrays.asList(this.outputMessage.getDataAsArrayOfStrings());
		List<ZObject> databases = new LinkedList<>();
		for (int i = 1; !response.get(i).trim().startsWith("*"); i++) {
			Pattern pattern = Pattern.compile("(\\w+)\\s+(\\w+.+)");
			Matcher matcher = pattern.matcher(response.get(i).trim());
			if (matcher.matches()) {
				ZObject database = new ZObject();
				database.setName(matcher.group(1));
				database.setType("IMS_DATABASE");
				database.setStatus(matcher.group(2));
				databases.add(database);
			}
		}
		return databases;
	}

	public List<ZObject> getProgramStatuses() throws Exception {
		this.execute("/DIS STATUS PROGRAM");
		List<String> response = Arrays.asList(this.outputMessage.getDataAsArrayOfStrings());
		List<ZObject> programs = new LinkedList<>();
		for (int i = 1; !response.get(i).trim().startsWith("*"); i++) {
			Pattern pattern = Pattern.compile("(\\w+)\\s+(\\w+.+)");
			Matcher matcher = pattern.matcher(response.get(i).trim());
			if (matcher.matches()) {
				ZObject program = new ZObject();
				program.setName(matcher.group(1));
				program.setType("IMS_PROGRAM");
				program.setStatus(matcher.group(2));
				programs.add(program);
			}
		}
		return programs;
	}

	public List<ZObject> getActiveRegions() throws Exception {
		this.execute("/DISPLAY ACT REGION");
		List<String> response = Arrays.asList(this.outputMessage.getDataAsArrayOfStrings());
		List<ZObject> activeRegions = new LinkedList<>();
		for (String responseLine : response) {
			if (responseLine.contains(" TP")) {
				ZObject activeRegion = new ZObject();
				activeRegion.setName(responseLine.replaceFirst("\\s+", "").split("\\s+")[1]);
				activeRegion.setType("IMS_REGION");
				activeRegion.setStatus("");
				activeRegions.add(activeRegion);
			} else if (responseLine.contains(" BMP")) {
				ZObject activeRegion = new ZObject();
				activeRegion.setName(responseLine.replaceFirst("\\s+", "").split("\\s+")[1]);
				activeRegion.setType("IMS_BMP");
				activeRegion.setStatus("");
				activeRegions.add(activeRegion);
			}
		}
		return activeRegions;
	}

	public List<ImsExternalSubsystem> getExternalSubsystems() throws Exception {
		this.execute("/DISPLAY SUBSYS ALL");
		List<String> response = Arrays.asList(this.outputMessage.getDataAsArrayOfStrings());
		List<ImsExternalSubsystem> imsExternalSubsystems = new LinkedList<>();
		for (String responseLine : response) {
			if (!responseLine.substring(4, 5).equals(" ") && !responseLine.substring(4, 5).equals("*")
					&& !responseLine.startsWith("*")) {
				ImsExternalSubsystem imsExternalSubsystem = new ImsExternalSubsystem();
				imsExternalSubsystem.setName(responseLine.substring(4, 8));
				imsExternalSubsystem.setStatus(responseLine.substring(41, responseLine.length() - 1));
				imsExternalSubsystems.add(imsExternalSubsystem);
			}
		}
		return imsExternalSubsystems;
	}

	public List<ImsQueuedMessage> getQueuedMessages() throws Exception {
		this.execute("/DISPLAY Q TRAN");
		List<String> response = Arrays.asList(this.outputMessage.getDataAsArrayOfStrings());
		List<ImsQueuedMessage> imsQueuedMessages = new LinkedList<>();
		for (String responseLine : response) {
			if (!responseLine.contains("PSBNAME") && !responseLine.substring(4, 5).equals("*")
					&& !responseLine.startsWith("*")) {
				String[] responseColumns = responseLine.trim().split("\\s+");
				ImsQueuedMessage imsQueuedMessage = new ImsQueuedMessage();
				imsQueuedMessage.setMessageCount(Integer.valueOf(responseColumns[2]));
				imsQueuedMessage.setTransactionName(responseColumns[3]);
				imsQueuedMessage.setPsbName(responseColumns[4]);
				imsQueuedMessages.add(imsQueuedMessage);
			} else if (responseLine.contains("*NO QUEUES*")) {
				imsQueuedMessages.clear();
				break;
			}
		}
		return imsQueuedMessages;
	}
}
