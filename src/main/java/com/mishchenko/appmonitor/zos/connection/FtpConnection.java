package com.mishchenko.appmonitor.zos.connection;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.mishchenko.appmonitor.zos.util.ZConnectionParameters;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FtpConnection {

	private static final Logger log = LoggerFactory.getLogger(FtpConnection.class);

	protected static final String FTP_FILETYPE_SEQ = "filetype=seq";

	protected FTPClient ftpClient;

	protected String hostname;

	protected String usedId;

	protected String password;

	public FtpConnection(ZConnectionParameters connectionParameters) {
		this.ftpClient = new FTPClient();
		this.hostname = connectionParameters.getHostname();
		this.usedId = connectionParameters.getUserId();
		this.password = connectionParameters.getPassword();
	}

	public void connect() throws Exception {
		ftpClient.connect(this.hostname);
		int replyCode = ftpClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(replyCode)) {
			ftpClient.disconnect();
			throw new Exception("Could not connect to server. FTP reply code: " + replyCode);
		} else {
			log.info("FTP client connected to: {}. FTP reply code: {}", this.hostname, replyCode);
		}
	}

	public void login() throws Exception {
		if (!ftpClient.isConnected()) {
			connect();
		}
		ftpClient.login(this.usedId, this.password);
		int replyCode = ftpClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(replyCode)) {
			ftpClient.logout();
			throw new Exception("Could not login to server. FTP reply code: " + replyCode, null);
		} else {
			log.info("FTP client login successfull to: {}. FTP reply code: {}", this.hostname, replyCode);
		}
	}

	public void disconnect() throws Exception {
		ftpClient.disconnect();
		log.info("FTP client disconnected from: {}", this.hostname);
	}

	public List<String> retrieveFile(String remoteFileName) throws Exception {
		FTPClientConfig ftpClientConfig = new FTPClientConfig("org.apache.commons.net.ftp.parser.MVSFTPEntryParser");
		ftpClient.configure(ftpClientConfig);

		ftpClient.site(FTP_FILETYPE_SEQ);
		InputStream retrieveFileStream = ftpClient.retrieveFileStream(remoteFileName);
		if (retrieveFileStream == null) {
			throw new Exception("No such file on remote: " + remoteFileName);
		}

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((retrieveFileStream)));
		List<String> retrievedLines = new ArrayList<>();
		while (true) {
			String readLine = bufferedReader.readLine();
			if (readLine != null) {
				retrievedLines.add(readLine);
			} else {
				retrieveFileStream.close();
				bufferedReader.close();
				break;
			}
		}
		ftpClient.completePendingCommand();
		return retrievedLines;
	}
}
