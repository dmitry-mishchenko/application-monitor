package com.mishchenko.appmonitor.zos.util;

import com.mishchenko.appmonitor.domain.Subsystem;

public class ZConnectionParameters {

    /**
     * Subsystem for this parameters entity
     */
    private Subsystem subsystem;

    /**
     * LPAR name or IP to connect
     */
    private String hostname;

    /**
     * Port for connection
     */
    private String port;

    /**
     * UserID
     */
    private String userId;

    /**
     * User password
     */
    private String password;

    /**
     * IMS datastore
     */
    private String imsDatastore;

    /**
     * IMS exit identifier
     */
    private String imsExitIdentifier;

    /**
     * Job accounting info parameter (is used in JCL)
     */
    private String jobAccountingInfo;

    /**
     * Job class parameter (is used in JCL)
     */
    private String jobClass;

    public Subsystem getSubsystem() {
        return subsystem;
    }

    public void setSubsystem(Subsystem subsystem) {
        this.subsystem = subsystem;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImsDatastore() {
        return imsDatastore;
    }

    public void setImsDatastore(String imsDatastore) {
        this.imsDatastore = imsDatastore;
    }

    public String getImsExitIdentifier() {
        return imsExitIdentifier;
    }

    public void setImsExitIdentifier(String imsExitIdentifier) {
        this.imsExitIdentifier = imsExitIdentifier;
    }

    public String getJobAccountingInfo() {
        return jobAccountingInfo;
    }

    public void setJobAccountingInfo(String jobAccountingInfo) {
        this.jobAccountingInfo = jobAccountingInfo;
    }

    public String getJobClass() {
        return jobClass;
    }

    public void setJobClass(String jobClass) {
        this.jobClass = jobClass;
    }
}
