package com.mishchenko.appmonitor.repository;

import com.mishchenko.appmonitor.domain.ObjectStatus;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ObjectStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ObjectStatusRepository extends JpaRepository<ObjectStatus, Long> {

}
