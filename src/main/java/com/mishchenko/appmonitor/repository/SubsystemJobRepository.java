package com.mishchenko.appmonitor.repository;

import java.util.List;

import com.mishchenko.appmonitor.domain.SubsystemJob;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SubsystemJob entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubsystemJobRepository extends JpaRepository<SubsystemJob, Long> {

    @Query("select subsystemJob from SubsystemJob subsystemJob where subsystemJob.subsystem.id =:id")
    List<SubsystemJob> findAllBySubsystemId(@Param("id") Long id);
}
