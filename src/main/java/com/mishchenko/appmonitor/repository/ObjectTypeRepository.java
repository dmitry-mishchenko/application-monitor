package com.mishchenko.appmonitor.repository;

import com.mishchenko.appmonitor.domain.ObjectType;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the ObjectType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ObjectTypeRepository extends JpaRepository<ObjectType, Long> {

}
