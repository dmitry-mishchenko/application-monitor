package com.mishchenko.appmonitor.repository;

import com.mishchenko.appmonitor.domain.Subsystem;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Subsystem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubsystemRepository extends JpaRepository<Subsystem, Long> {

}
