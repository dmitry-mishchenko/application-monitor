package com.mishchenko.appmonitor.repository;

import java.util.List;

import com.mishchenko.appmonitor.domain.SubsystemObject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SubsystemObject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubsystemObjectRepository extends JpaRepository<SubsystemObject, Long> {

    @Query("select subsystemObject from SubsystemObject subsystemObject where subsystemObject.subsystem.id = :id")
    List<SubsystemObject> findAllBySubsystemId(@Param("id") Long id);
}
