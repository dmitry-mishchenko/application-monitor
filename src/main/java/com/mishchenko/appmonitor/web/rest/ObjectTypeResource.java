package com.mishchenko.appmonitor.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mishchenko.appmonitor.service.ObjectTypeService;
import com.mishchenko.appmonitor.web.rest.errors.BadRequestAlertException;
import com.mishchenko.appmonitor.web.rest.util.HeaderUtil;
import com.mishchenko.appmonitor.service.dto.ObjectTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ObjectType.
 */
@RestController
@RequestMapping("/api")
public class ObjectTypeResource {

    private final Logger log = LoggerFactory.getLogger(ObjectTypeResource.class);

    private static final String ENTITY_NAME = "objectType";

    private final ObjectTypeService objectTypeService;

    public ObjectTypeResource(ObjectTypeService objectTypeService) {
        this.objectTypeService = objectTypeService;
    }

    /**
     * POST  /object-types : Create a new objectType.
     *
     * @param objectTypeDTO the objectTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new objectTypeDTO, or with status 400 (Bad Request) if the objectType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/object-types")
    @Timed
    public ResponseEntity<ObjectTypeDTO> createObjectType(@RequestBody ObjectTypeDTO objectTypeDTO) throws URISyntaxException {
        log.debug("REST request to save ObjectType : {}", objectTypeDTO);
        if (objectTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new objectType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ObjectTypeDTO result = objectTypeService.save(objectTypeDTO);
        return ResponseEntity.created(new URI("/api/object-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /object-types : Updates an existing objectType.
     *
     * @param objectTypeDTO the objectTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated objectTypeDTO,
     * or with status 400 (Bad Request) if the objectTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the objectTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/object-types")
    @Timed
    public ResponseEntity<ObjectTypeDTO> updateObjectType(@RequestBody ObjectTypeDTO objectTypeDTO) throws URISyntaxException {
        log.debug("REST request to update ObjectType : {}", objectTypeDTO);
        if (objectTypeDTO.getId() == null) {
            return createObjectType(objectTypeDTO);
        }
        ObjectTypeDTO result = objectTypeService.save(objectTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, objectTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /object-types : get all the objectTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of objectTypes in body
     */
    @GetMapping("/object-types")
    @Timed
    public List<ObjectTypeDTO> getAllObjectTypes() {
        log.debug("REST request to get all ObjectTypes");
        return objectTypeService.findAll();
        }

    /**
     * GET  /object-types/:id : get the "id" objectType.
     *
     * @param id the id of the objectTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the objectTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/object-types/{id}")
    @Timed
    public ResponseEntity<ObjectTypeDTO> getObjectType(@PathVariable Long id) {
        log.debug("REST request to get ObjectType : {}", id);
        ObjectTypeDTO objectTypeDTO = objectTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(objectTypeDTO));
    }

    /**
     * DELETE  /object-types/:id : delete the "id" objectType.
     *
     * @param id the id of the objectTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/object-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteObjectType(@PathVariable Long id) {
        log.debug("REST request to delete ObjectType : {}", id);
        objectTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
