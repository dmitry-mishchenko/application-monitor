package com.mishchenko.appmonitor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.codahale.metrics.annotation.Timed;
import com.mishchenko.appmonitor.service.IssueService;
import com.mishchenko.appmonitor.service.dto.IssueDTO;
import com.mishchenko.appmonitor.web.rest.errors.BadRequestAlertException;
import com.mishchenko.appmonitor.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Issue.
 */
@RestController
@RequestMapping("/api")
public class IssueResource {

    private final Logger log = LoggerFactory.getLogger(IssueResource.class);

    private static final String ENTITY_NAME = "issue";

    private final IssueService issueService;

    public IssueResource(IssueService issueService) {
        this.issueService = issueService;
    }

    /**
     * POST /issues : Create a new issue.
     *
     * @param issueDTO the issueDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         issueDTO, or with status 400 (Bad Request) if the issue has already
     *         an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/issues")
    @Timed
    public ResponseEntity<IssueDTO> createIssue(@RequestBody IssueDTO issueDTO) throws URISyntaxException {
        log.debug("REST request to save Issue : {}", issueDTO);
        if (issueDTO.getId() != null) {
            throw new BadRequestAlertException("A new issue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IssueDTO result = issueService.save(issueDTO);
        return ResponseEntity.created(new URI("/api/issues/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT /issues : Updates an existing issue.
     *
     * @param issueDTO the issueDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         issueDTO, or with status 400 (Bad Request) if the issueDTO is not
     *         valid, or with status 500 (Internal Server Error) if the issueDTO
     *         couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/issues")
    @Timed
    public ResponseEntity<IssueDTO> updateIssue(@RequestBody IssueDTO issueDTO) throws URISyntaxException {
        log.debug("REST request to update Issue : {}", issueDTO);
        if (issueDTO.getId() == null) {
            return createIssue(issueDTO);
        }
        IssueDTO result = issueService.save(issueDTO);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, issueDTO.getId().toString()))
                .body(result);
    }

    /**
     * GET /issues : get all the issues.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of issues in
     *         body
     */
    @GetMapping("/issues")
    @Timed
    public List<IssueDTO> getAllIssues() {
        log.debug("REST request to get all Issues");
        return issueService.findAll();
    }

    /**
     * GET /issues/:id : get the "id" issue.
     *
     * @param id the id of the issueDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the issueDTO,
     *         or with status 404 (Not Found)
     */
    @GetMapping("/issues/{id}")
    @Timed
    public ResponseEntity<IssueDTO> getIssue(@PathVariable Long id) {
        log.debug("REST request to get Issue : {}", id);
        IssueDTO issueDTO = issueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(issueDTO));
    }

    @GetMapping("/issues/getBySubsystem/{id}")
    @Timed
    public List<IssueDTO> getIssuesBySubsystem(@PathVariable Long id) {
        log.debug("REST request to get Issue for Subsystem : {}", id);
        return issueService.findAllBySubsystemId(id);
    }

    /**
     * DELETE /issues/:id : delete the "id" issue.
     *
     * @param id the id of the issueDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/issues/{id}")
    @Timed
    public ResponseEntity<Void> deleteIssue(@PathVariable Long id) {
        log.debug("REST request to delete Issue : {}", id);
        issueService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * POST /issues/:id/executeCommand : execute command.
     *
     */
    @PostMapping("/issues/{id}/executeCommand")
    @Timed
    @Transactional
    public ResponseEntity<String> executeCommand(@PathVariable Long id, @RequestBody String command) throws Exception {
        log.debug("REST request to execute command to Issue : {}", id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(this.issueService.executeCommand(id, command)));
    }
}
