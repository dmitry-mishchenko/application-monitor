package com.mishchenko.appmonitor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import com.codahale.metrics.annotation.Timed;
import com.mishchenko.appmonitor.service.SubsystemService;
import com.mishchenko.appmonitor.service.dto.SubsystemDTO;
import com.mishchenko.appmonitor.web.rest.errors.BadRequestAlertException;
import com.mishchenko.appmonitor.web.rest.util.HeaderUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Subsystem.
 */
@RestController
@RequestMapping("/api")
public class SubsystemResource {

    private final Logger log = LoggerFactory.getLogger(SubsystemResource.class);

    private static final String ENTITY_NAME = "subsystem";

    private final SubsystemService subsystemService;

    public SubsystemResource(SubsystemService subsystemService) {
        this.subsystemService = subsystemService;
    }

    /**
     * POST /subsystems : Create a new subsystem.
     *
     * @param subsystemDTO the subsystemDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new
     *         subsystemDTO, or with status 400 (Bad Request) if the subsystem has
     *         already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/subsystems")
    @Timed
    public ResponseEntity<SubsystemDTO> createSubsystem(@RequestBody SubsystemDTO subsystemDTO)
            throws URISyntaxException {
        log.debug("REST request to save Subsystem : {}", subsystemDTO);
        if (subsystemDTO.getId() != null) {
            throw new BadRequestAlertException("A new subsystem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubsystemDTO result = subsystemService.save(subsystemDTO);
        return ResponseEntity.created(new URI("/api/subsystems/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

    /**
     * PUT /subsystems : Updates an existing subsystem.
     *
     * @param subsystemDTO the subsystemDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated
     *         subsystemDTO, or with status 400 (Bad Request) if the subsystemDTO is
     *         not valid, or with status 500 (Internal Server Error) if the
     *         subsystemDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/subsystems")
    @Timed
    public ResponseEntity<SubsystemDTO> updateSubsystem(@RequestBody SubsystemDTO subsystemDTO)
            throws URISyntaxException {
        log.debug("REST request to update Subsystem : {}", subsystemDTO);
        if (subsystemDTO.getId() == null) {
            return createSubsystem(subsystemDTO);
        }
        SubsystemDTO result = subsystemService.save(subsystemDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subsystemDTO.getId().toString())).body(result);
    }

    /**
     * GET /subsystems : get all the subsystems.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of subsystems in
     *         body
     */
    @GetMapping("/subsystems")
    @Timed
    public List<SubsystemDTO> getAllSubsystems() {
        log.debug("REST request to get all Subsystems");
        return subsystemService.findAll();
    }

    /**
     * GET /subsystems/:id : get the "id" subsystem.
     *
     * @param id the id of the subsystemDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the
     *         subsystemDTO, or with status 404 (Not Found)
     */
    @GetMapping("/subsystems/{id}")
    @Timed
    public ResponseEntity<SubsystemDTO> getSubsystem(@PathVariable Long id) {
        log.debug("REST request to get Subsystem : {}", id);
        SubsystemDTO subsystemDTO = subsystemService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(subsystemDTO));
    }

    /**
     * DELETE /subsystems/:id : delete the "id" subsystem.
     *
     * @param id the id of the subsystemDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/subsystems/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubsystem(@PathVariable Long id) {
        log.debug("REST request to delete Subsystem : {}", id);
        subsystemService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET /subsystems/updateIssues/:id : update issues in the "id" subsystem.
     *
     * @param id the id of the subsystemDTO to update issues
     * @return the ResponseEntity with status 200 (OK)
     */
    @GetMapping("/subsystems/updateIssues/{id}")
    @Timed
    public ResponseEntity<Void> updateIssues(@PathVariable Long id) {
        log.debug("REST request to update issues in Subsystem : {}", id);
        subsystemService.updateIssues(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * GET /subsystems/updateInformation/:id : update information of the "id"
     * subsystem, including subsystem jobs.
     *
     * @param id the id of the subsystemDTO to update information
     * @return the ResponseEntity with status 200 (OK)
     */
    @GetMapping("/subsystems/updateInformation/{id}")
    @Timed
    public ResponseEntity<Void> updateInformation(@PathVariable Long id) {
        log.debug("REST request to update information of Subsystem : {}", id);
        subsystemService.updateInformation(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
