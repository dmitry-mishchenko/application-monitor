package com.mishchenko.appmonitor.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mishchenko.appmonitor.service.SubsystemObjectService;
import com.mishchenko.appmonitor.web.rest.errors.BadRequestAlertException;
import com.mishchenko.appmonitor.web.rest.util.HeaderUtil;
import com.mishchenko.appmonitor.web.rest.util.PaginationUtil;
import com.mishchenko.appmonitor.service.dto.SubsystemObjectDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SubsystemObject.
 */
@RestController
@RequestMapping("/api")
public class SubsystemObjectResource {

    private final Logger log = LoggerFactory.getLogger(SubsystemObjectResource.class);

    private static final String ENTITY_NAME = "subsystemObject";

    private final SubsystemObjectService subsystemObjectService;

    public SubsystemObjectResource(SubsystemObjectService subsystemObjectService) {
        this.subsystemObjectService = subsystemObjectService;
    }

    /**
     * POST  /subsystem-objects : Create a new subsystemObject.
     *
     * @param subsystemObjectDTO the subsystemObjectDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subsystemObjectDTO, or with status 400 (Bad Request) if the subsystemObject has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/subsystem-objects")
    @Timed
    public ResponseEntity<SubsystemObjectDTO> createSubsystemObject(@RequestBody SubsystemObjectDTO subsystemObjectDTO) throws URISyntaxException {
        log.debug("REST request to save SubsystemObject : {}", subsystemObjectDTO);
        if (subsystemObjectDTO.getId() != null) {
            throw new BadRequestAlertException("A new subsystemObject cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubsystemObjectDTO result = subsystemObjectService.save(subsystemObjectDTO);
        return ResponseEntity.created(new URI("/api/subsystem-objects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /subsystem-objects : Updates an existing subsystemObject.
     *
     * @param subsystemObjectDTO the subsystemObjectDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subsystemObjectDTO,
     * or with status 400 (Bad Request) if the subsystemObjectDTO is not valid,
     * or with status 500 (Internal Server Error) if the subsystemObjectDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/subsystem-objects")
    @Timed
    public ResponseEntity<SubsystemObjectDTO> updateSubsystemObject(@RequestBody SubsystemObjectDTO subsystemObjectDTO) throws URISyntaxException {
        log.debug("REST request to update SubsystemObject : {}", subsystemObjectDTO);
        if (subsystemObjectDTO.getId() == null) {
            return createSubsystemObject(subsystemObjectDTO);
        }
        SubsystemObjectDTO result = subsystemObjectService.save(subsystemObjectDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, subsystemObjectDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /subsystem-objects : get all the subsystemObjects.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of subsystemObjects in body
     */
    @GetMapping("/subsystem-objects")
    @Timed
    public ResponseEntity<List<SubsystemObjectDTO>> getAllSubsystemObjects(Pageable pageable) {
        log.debug("REST request to get a page of SubsystemObjects");
        Page<SubsystemObjectDTO> page = subsystemObjectService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subsystem-objects");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /subsystem-objects/:id : get the "id" subsystemObject.
     *
     * @param id the id of the subsystemObjectDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subsystemObjectDTO, or with status 404 (Not Found)
     */
    @GetMapping("/subsystem-objects/{id}")
    @Timed
    public ResponseEntity<SubsystemObjectDTO> getSubsystemObject(@PathVariable Long id) {
        log.debug("REST request to get SubsystemObject : {}", id);
        SubsystemObjectDTO subsystemObjectDTO = subsystemObjectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(subsystemObjectDTO));
    }

    /**
     * DELETE  /subsystem-objects/:id : delete the "id" subsystemObject.
     *
     * @param id the id of the subsystemObjectDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/subsystem-objects/{id}")
    @Timed
    public ResponseEntity<Void> deleteSubsystemObject(@PathVariable Long id) {
        log.debug("REST request to delete SubsystemObject : {}", id);
        subsystemObjectService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
