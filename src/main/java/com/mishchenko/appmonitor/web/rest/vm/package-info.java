/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mishchenko.appmonitor.web.rest.vm;
