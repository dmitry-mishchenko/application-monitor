/**
 * Data Access Objects used by WebSocket services.
 */
package com.mishchenko.appmonitor.web.websocket.dto;
